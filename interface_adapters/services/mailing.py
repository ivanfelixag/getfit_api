#!/usr/bin/env python
# -*- coding: utf-8 -*-
from abc import abstractmethod
from external_interfaces.services.mailing import *

class BaseMailingService(object):

    @abstractmethod
    def get_strategy(self): raise NotImplementedError

    def execute(self):
        strategy = self.get_strategy()
        mailing_service = MailingService(strategy)
        mailing_service.execute()


class NewUserMailingService(BaseMailingService):

    def set_params(self, user):
        self.user = user
        return self

    def get_strategy(self):
        strategy = NewUserMailingStrategy()
        recipient = MailingService.Recipient(
            self.user.full_name,
            self.user.email
        )
        strategy.set_params(recipient)
        return strategy


class DeleteClassMailingService(BaseMailingService):

    def set_params(self, users, class_name):
        self.users = users
        self.class_name = class_name
        return self

    def get_strategy(self):
        strategy = DeleteClassMailingStrategy()
        recipients = []
        for user in self.users:
            recipients.append(MailingService.Recipient(
                user.full_name,
                user.email
            ))
        strategy.set_params(recipients, self.class_name)
        return strategy


class PermanentChangeTimeSessionMailingService(BaseMailingService):

    def set_params(self, users, session, class_name):
        self.users = users
        self.session = session
        self.class_name = class_name
        return self

    def get_strategy(self):
        strategy = PermanentChangeTimeSessionMailingStrategy()
        recipients = []
        for user in self.users:
            recipients.append(MailingService.Recipient(
                user.full_name,
                user.email
            ))
        strategy.set_params(recipients, self.session.day, self.session.start_time,
            self.session.end_time, self.class_name)
        return strategy


class DeleteSessionMailingService(BaseMailingService):

    def set_params(self, users, session, class_name):
        self.users = users
        self.session = session
        self.class_name = class_name
        return self

    def get_strategy(self):
        strategy = DeleteSessionMailingStrategy()
        recipients = []
        for user in self.users:
            recipients.append(MailingService.Recipient(
                user.full_name,
                user.email
            ))
        strategy.set_params(recipients, self.session.day, self.session.start_time,
            self.session.end_time, self.class_name)
        return strategy


class RequestNonAssistanceMailingService(BaseMailingService):

    def set_params(self, manager_users, trainer_user):
        self.manager_users = manager_users
        self.trainer_user = trainer_user
        return self

    def get_strategy(self):
        strategy = RequestNonAssistanceMailingStrategy()
        recipients = []
        for manager_user in self.manager_users:
            recipients.append(MailingService.Recipient(
                manager_user.full_name,
                manager_user.email
            ))
        strategy.set_params(recipients, self.trainer_user.full_name,
            self.trainer_user.email, self.trainer_user.identification)
        return strategy


class ReportRequestNonAssistanceApprovedMailingService(BaseMailingService):

    def set_params(self, trainer_user):
        self.trainer_user = trainer_user
        return self

    def get_strategy(self):
        strategy = ReportRequestNonAssistanceApprovedMailingStrategy()
        recipient = MailingService.Recipient(
            self.trainer_user.full_name,
            self.trainer_user.email
        )
        strategy.set_params(recipient)
        return strategy


class ReportRequestNonAssistanceDeniedMailingService(BaseMailingService):

    def set_params(self, trainer_user):
        self.trainer_user = trainer_user
        return self

    def get_strategy(self):
        strategy = ReportRequestNonAssistanceDeniedMailingStrategy()
        recipient = MailingService.Recipient(
            self.trainer_user.full_name,
            self.trainer_user.email
        )
        strategy.set_params(recipient)
        return strategy


class NewTrainerContractMailingService(BaseMailingService):

    def set_params(self, trainer_user):
        self.trainer_user = trainer_user
        return self

    def get_strategy(self):
        strategy = NewTrainerContractMailingStrategy()
        recipient = MailingService.Recipient(
            self.trainer_user.full_name,
            self.trainer_user.email
        )
        strategy.set_params(recipient)
        return strategy
