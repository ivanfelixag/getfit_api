#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
from external_interfaces.core.session.repositories.HistoricalPeriodTrainerSessionDatabaseRepo import HistoricalPeriodTrainerSessionDatabaseRepoFactory

class HistoricalPeriodTrainerSessionRepo(object):

    def __init__(self, db_repo):
        self.db_repo = db_repo
    
    def register_historical_period(self, session_id, trainer_user_identification, start_date=None, end_date=None, temporal_substitution=False):
        return self.db_repo.register_historical_period(
            session_id,
            trainer_user_identification,
            start_date if start_date else datetime.now(),
            end_date,
            temporal_substitution
        )

class HistoricalPeriodTrainerSessionRepoFactory(object):
    
    @staticmethod
    def get():
        db_repo = HistoricalPeriodTrainerSessionDatabaseRepoFactory.get()
        return HistoricalPeriodTrainerSessionRepo(db_repo)