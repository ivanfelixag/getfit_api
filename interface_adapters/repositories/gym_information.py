#!/usr/bin/env python
# -*- coding: utf-8 -*-
from external_interfaces.core.gym_information.repositories.GymInformationDatabaseRepo import GymInformationDatabaseRepoFactory

class GymInformationRepo(object):

    def __init__(self, db_repo):
        self.db_repo = db_repo

    def get_all_gym_informations(self):
        return self.db_repo.get_all_gym_informations()


class GymInformationRepoFactory(object):

    @staticmethod
    def get():
        db_repo = GymInformationDatabaseRepoFactory.get()
        return GymInformationRepo(db_repo)