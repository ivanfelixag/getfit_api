#!/usr/bin/env python
# -*- coding: utf-8 -*-
from external_interfaces.core.user.repositories.UserDatabaseRepo import UserDatabaseRepoFactory

class UserRepo(object):
    TRAINER_ROLE = 'trainer'
    MANAGER_ROLE = 'manager'
    PLATFORM_ADMINISTRATOR_ROLE = 'platform_administrator'
    def __init__(self, db_repo):
        self.db_repo = db_repo

    def create_user(self, first_name, last_name, email, identification, password, phone_number):
        return self.db_repo.create_user(first_name, last_name, email, identification, password, phone_number)
    
    def disable_user(self, identification):
        return self.db_repo.disable_user(identification)

    def get_user_by_identification(self, identification):
        return self.db_repo.get_user_by_identification(identification)

    def exists_user_identification(self, identification, check_active=True):
        return self.db_repo.exists_user_identification(identification, check_active)

    def get_trainers_users(self, active):
        return self.db_repo.get_trainers_users(active)

    def user_has_role(self, identification, role):
        return self.db_repo.user_has_role(identification, role)

class UserRepoFactory(object):

    @staticmethod
    def get():
        db_repo = UserDatabaseRepoFactory.get()
        return UserRepo(db_repo)