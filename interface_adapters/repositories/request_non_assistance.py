#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
from external_interfaces.core.user.repositories.RequestNonAssistanceDatabaseRepo import RequestNonAssistanceDatabaseRepoFactory

class RequestNonAssistanceRepo(object):

    PENDING_STATUS = 'pending'
    APPROVED_STATUS = 'approved'
    DENIED_STATUS = 'denied'

    def __init__(self, db_repo):
        self.db_repo = db_repo
    
    def get_request_non_assistance_by_id(self, id):
        return self.db_repo.get_request_non_assistance_by_id(id)

    def register_request_non_assistance(self, trainer_user_identification, start_at, end_at, comment, status):
        return self.db_repo.register_request_non_assistance(
            trainer_user_identification, 
            start_at, 
            end_at, 
            comment,
            status
        )

    def update_status_request_non_assistance(self, id, status):
        self.db_repo.update_status_request_non_assistance(id, status)
    
    def get_requests_non_assistance(self, status):
        return self.db_repo.get_requests_non_assistance(status)

class RequestNonAssistanceRepoFactory(object):
    
    @staticmethod
    def get():
        db_repo = RequestNonAssistanceDatabaseRepoFactory.get()
        return RequestNonAssistanceRepo(db_repo)