#!/usr/bin/env python
# -*- coding: utf-8 -*-
from external_interfaces.core.session.repositories.SessionDatabaseRepo import SessionDatabaseRepoFactory

class SessionRepo(object):
    MONDAY = 'MON'
    TUESDAY = 'TUES'
    WEDNESDAY = 'WED'
    THURSDAY = 'THURS'
    FRIDAY = 'FRI'
    SATURDAY = 'SAT'
    SUNDAY = 'SUN'
    DAYS = [MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY]

    MINIMUM_MINUTES = 30

    def __init__(self, db_repo):
        self.db_repo = db_repo

    def get_session_by_id(self, id):
        return self.db_repo.get_session_by_id(id)

    def update_session_capacity(self, id, capacity):
        return self.db_repo.update_session_capacity(id, capacity)

    def exists_session_by_id(self, id):
        return self.db_repo.exists_session_by_id(id)

    def register_participant(self, id, user_identification):
        self.db_repo.register_participant(id, user_identification)
    
    def unsubscribe_participant(self, id, user_identification):
        self.db_repo.unsubscribe_participant(id, user_identification)
    
    def exists_participant_session(self, id, user_identification):
        return self.db_repo.exists_participant_session(id, user_identification)

    def register_session(self, day, start_time, end_time, capacity, trainer_user_identification, class_id):
        return self.db_repo.register_session(day, start_time, end_time, capacity, trainer_user_identification, class_id)

class SessionRepoFactory(object):

    @staticmethod
    def get():
        db_repo = SessionDatabaseRepoFactory.get()
        return SessionRepo(db_repo)