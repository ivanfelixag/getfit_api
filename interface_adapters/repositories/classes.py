#!/usr/bin/env python
# -*- coding: utf-8 -*-
from external_interfaces.core.classes.repositories.ClassDatabaseRepo import ClassDatabaseRepoFactory

class ClassRepo(object):

    def __init__(self, db_repo):
        self.db_repo = db_repo

    def register_class(self, description, active):
        return self.db_repo.register_class(description, active)

    def exists_class_by_id(self, class_id):
        return self.db_repo.exists_class_by_id(class_id)

    def get_all_classes(self):
        return self.db_repo.get_all_classes()
    
    def get_user_classes(self, user_identification):
        return self.db_repo.get_user_classes(user_identification)
    
    def get_trainer_user_classes(self, user_identification):
        return self.db_repo.get_trainer_user_classes(user_identification)

class ClassRepoFactory(object):

    @staticmethod
    def get():
        db_repo = ClassDatabaseRepoFactory.get()
        return ClassRepo(db_repo)