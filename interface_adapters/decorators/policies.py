#!/usr/bin/env python
# -*- coding: utf-8 -*-
from domain.validators.user.UserHasRoleValidator import UserHasRoleValidatorFactory

################################
# User has role
################################
class UserHasRoleDecorator(object):
    
    def __init__(self, user_has_role_validator):
        self.user_has_role_validator = user_has_role_validator
    
    def set_params(self, identification, role):
        self.identification = identification
        self.role = role
        return self
    
    def execute(self):
        self.user_has_role_validator.set_params(self.identification, self.role).execute()

class UserHasRoleDecoratorFactory(object):

    @staticmethod
    def get():
        user_has_role_validator = UserHasRoleValidatorFactory.get()
        return UserHasRoleDecorator(user_has_role_validator)