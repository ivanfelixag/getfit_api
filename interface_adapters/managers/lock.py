#!/usr/bin/env python
# -*- coding: utf-8 -*-

from common.interfaces.ILock import ILock
from external_interfaces.managers.lock import LockManager as FrameworkLockManager

class LockManager(ILock):

    @classmethod
    def begin_lock(cls, name):
        return FrameworkLockManager.begin_lock(name)