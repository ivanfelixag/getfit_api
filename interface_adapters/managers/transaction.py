#!/usr/bin/env python
# -*- coding: utf-8 -*-
from external_interfaces.managers.transaction import TransactionManager as FrameworkTransactionManager
from common.interfaces.ITransaction import ITransaction

class TransactionManager(ITransaction):

    @classmethod
    def begin_transaction(cls):
        FrameworkTransactionManager.begin_transaction()

    @classmethod
    def do_roolback(cls):
        FrameworkTransactionManager.do_roolback()
    
    @classmethod
    def do_commit(cls):
        FrameworkTransactionManager.do_commit()
    
    @classmethod
    def end_transaction(cls):
        FrameworkTransactionManager.end_transaction()