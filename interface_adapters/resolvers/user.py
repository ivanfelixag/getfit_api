#!/usr/bin/env python
# -*- coding: utf-8 -*-
from domain.use_cases.user.GetUserByIdentification import GetUserByIdentificationFactory
from domain.use_cases.user.GetTrainersUsers import GetTrainersUsersFactory
from domain.use_cases.user.GetRequestsNonAssistance import GetRequestsNonAssistanceFactory

################################
# Get user by identification
################################
class UserResolver(object):

    def __init__(self, get_user_by_identification_use_case):
        self.get_user_by_identification_use_case = get_user_by_identification_use_case

    def set_params(self, identification):
        self.identification = identification
        return self
    
    def execute(self):
        return self.get_user_by_identification_use_case.set_params(self.identification).execute()

class UserResolverFactory(object):

    @staticmethod
    def get():
        get_user_by_identification_use_case = GetUserByIdentificationFactory.get()
        return UserResolver(get_user_by_identification_use_case)


################################
# Get gym trainers
################################
class TrainersUsersResolver(object):

    def __init__(self, get_trainers_users_use_case):
        self.get_trainers_users_use_case = get_trainers_users_use_case
    
    def set_params(self, active):
        self.active = active
        return self

    def execute(self):
        return self.get_trainers_users_use_case.set_params(self.active).execute()

class TrainersUsersResolverFactory(object):

    @staticmethod
    def get():
        get_trainers_users_use_case = GetTrainersUsersFactory.get()
        return TrainersUsersResolver(get_trainers_users_use_case)


################################
# Get requests non assistance
################################
class RequestsNonAssistanceResolver(object):

    def __init__(self, get_requests_non_assistance_use_case):
        self.get_requests_non_assistance_use_case = get_requests_non_assistance_use_case
    
    def set_params(self, status):
        self.status = status
        return self

    def execute(self):
        return self.get_requests_non_assistance_use_case.set_params(self.status).execute()

class RequestsNonAssistanceResolverFactory(object):

    @staticmethod
    def get():
        get_requests_non_assistance_use_case = GetRequestsNonAssistanceFactory.get()
        return RequestsNonAssistanceResolver(get_requests_non_assistance_use_case)