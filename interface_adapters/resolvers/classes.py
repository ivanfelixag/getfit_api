#!/usr/bin/env python
# -*- coding: utf-8 -*-
from domain.use_cases.classes.GetClasses import GetClassesFactory
from domain.use_cases.classes.GetUserClasses import GetUserClassesFactory
from domain.use_cases.classes.GetTrainerUserClasses import GetTrainerUserClassesFactory

######################################
# Classes resolver
######################################
class ClassesResolver(object):

    def __init__(self, get_classes_use_case):
        self.get_classes_use_case = get_classes_use_case

    def execute(self):
        return self.get_classes_use_case.execute()

class ClassesResolverFactory(object):

    @staticmethod
    def get():
        get_classes_use_case = GetClassesFactory.get()
        return ClassesResolver(get_classes_use_case)


######################################
# Get user classes
######################################
class UserClassesResolver(object):

    def __init__(self, get_user_classes_use_case):
        self.get_user_classes_use_case = get_user_classes_use_case

    def set_params(self, user_identification):
        self.user_identification = user_identification
        return self

    def execute(self):
        return self.get_user_classes_use_case.set_params(self.user_identification).execute()

class UserClassesResolverFactory(object):

    @staticmethod
    def get():
        get_user_classes_use_case = GetUserClassesFactory.get()
        return UserClassesResolver(get_user_classes_use_case)


######################################
# Get trainer classes
######################################
class TrainerUserClassesResolver(object):

    def __init__(self, get_trainer_user_classes_use_case):
        self.get_trainer_user_classes_use_case = get_trainer_user_classes_use_case

    def set_params(self, user_identification):
        self.user_identification = user_identification
        return self

    def execute(self):
        return self.get_trainer_user_classes_use_case.set_params(self.user_identification).execute()

class TrainerUserClassesResolverFactory(object):

    @staticmethod
    def get():
        get_trainer_user_classes_use_case = GetTrainerUserClassesFactory.get()
        return TrainerUserClassesResolver(get_trainer_user_classes_use_case)