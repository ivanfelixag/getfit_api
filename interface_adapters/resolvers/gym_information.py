#!/usr/bin/env python
# -*- coding: utf-8 -*-
from domain.use_cases.gym_information.GetGymInformation import GetGymInformationFactory

class GymInformationResolver(object):

    def __init__(self, get_gym_information_use_case):
        self.get_gym_information_use_case = get_gym_information_use_case

    def execute(self):
        return self.get_gym_information_use_case.execute()


class GymInformationResolverFactory(object):

    @staticmethod
    def get():
        get_gym_information_use_case = GetGymInformationFactory.get()
        return GymInformationResolver(get_gym_information_use_case)