#!/usr/bin/env python
# -*- coding: utf-8 -*-
from domain.use_cases.session.RegisterSession import RegisterSessionFactory
from domain.use_cases.session.RegisterParticipantSession import RegisterParticipantSessionFactory
from domain.use_cases.session.UnsubscribeParticipantSession import UnsubscribeParticipantSessionFactory

################################
# Register session
################################
class RegisterSessionMutation(object):

    def __init__(self, register_session_use_case):
        self.register_session_use_case = register_session_use_case

    def set_params(self, day, start_time, end_time, capacity, trainer_user_identification, class_id):
        self.day = day
        self.start_time = start_time
        self.end_time = end_time
        self.capacity = capacity
        self.trainer_user_identification = trainer_user_identification
        self.class_id = class_id
        return self
    
    def execute(self):
        return self.register_session_use_case.set_params(self.day, self.start_time, self.end_time, 
                    self.capacity, self.trainer_user_identification, self.class_id).execute()

class RegisterSessionMutationFactory(object):

    @staticmethod
    def get():
        register_session_use_case = RegisterSessionFactory.get()
        return RegisterSessionMutation(register_session_use_case)


################################
# Register participant in class session
################################
class RegisterParticipantSessionMutation(object):

    def __init__(self, register_participant_session_use_case):
        self.register_participant_session_use_case = register_participant_session_use_case

    def set_params(self, session_id, user_identification):
        self.session_id = session_id
        self.user_identification = user_identification
        return self
    
    def execute(self):
        return self.register_participant_session_use_case.set_params(self.session_id, self.user_identification).execute()

class RegisterParticipantSessionMutationFactory(object):

    @staticmethod
    def get():
        register_participant_session_use_case = RegisterParticipantSessionFactory.get()
        return RegisterParticipantSessionMutation(register_participant_session_use_case)


################################
# Unsubscribe session user
################################
class UnsubscribeParticipantSessionMutation(object):

    def __init__(self, unsubscribe_participant_session_use_case):
        self.unsubscribe_participant_session_use_case = unsubscribe_participant_session_use_case

    def set_params(self, session_id, user_identification):
        self.session_id = session_id
        self.user_identification = user_identification
        return self
    
    def execute(self):
        return self.unsubscribe_participant_session_use_case.set_params(self.session_id, self.user_identification).execute()

class UnsubscribeParticipantSessionMutationFactory(object):

    @staticmethod
    def get():
        unsubscribe_participant_session_use_case = UnsubscribeParticipantSessionFactory.get()
        return UnsubscribeParticipantSessionMutation(unsubscribe_participant_session_use_case)