#!/usr/bin/env python
# -*- coding: utf-8 -*-
from domain.entities.user import User
from domain.entities.session import Session
from domain.use_cases.classes.RegisterClass import RegisterClassFactory

################################
# Register class
################################
class RegisterClassMutation(object):

    def __init__(self, register_class_use_case):
        self.register_class_use_case = register_class_use_case

    def set_params(self, description, sessions, active):
        self.description = description
        self.sessions = sessions
        self.active = active
        return self
    
    def execute(self):
        sessions = []
        for session in self.sessions:
            trainer_user = User(identification=session.trainer_user_identification)
            sessions.append(Session(None, session.day, session.start_time, session.end_time, session.capacity, trainer_user))
        return self.register_class_use_case.set_params(self.description, sessions, self.active).execute()

class RegisterClassMutationFactory(object):

    @staticmethod
    def get():
        register_class_use_case = RegisterClassFactory.get()
        return RegisterClassMutation(register_class_use_case)