#!/usr/bin/env python
# -*- coding: utf-8 -*-
from domain.use_cases.user.RegisterUser import RegisterUserFactory
from domain.use_cases.user.UnsubscribeUser import UnsubscribeUserFactory
from domain.use_cases.user.RegisterRequestNonAssistance import RegisterRequestNonAssistanceFactory
from domain.use_cases.user.ChangeStatusRequestNonAssistance import ChangeStatusRequestNonAssistanceFactory

################################
# Create user
################################
class RegisterUserMutation(object):

    def __init__(self, register_user_use_case):
        self.register_user_use_case = register_user_use_case

    def set_params(self, first_name, last_name, email, identification, password, phone_number):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.identification = identification
        self.password = password
        self.phone_number = phone_number
        return self
    
    def execute(self):
        return self.register_user_use_case.set_params(self.first_name, self.last_name, self.email, self.identification, self.password, self.phone_number).execute()

class RegisterUserMutationFactory(object):

    @staticmethod
    def get():
        register_user_use_case = RegisterUserFactory.get()
        return RegisterUserMutation(register_user_use_case)


################################
# Unsubscribe user
################################

class UnsubscribeUserMutation(object):

    def __init__(self, unsubscribe_user_use_case):
        self.unsubscribe_user_use_case = unsubscribe_user_use_case
    
    def set_params(self, identification):
        self.identification = identification
        return self
    
    def execute(self):
        self.unsubscribe_user_use_case.set_params(self.identification).execute()
    
class UnsubscribeUserMutationFactory(object):

    @staticmethod
    def get():
        unsubscribe_user_use_case = UnsubscribeUserFactory.get()
        return UnsubscribeUserMutation(unsubscribe_user_use_case)


################################
# Register Request Non Assistance
################################

class RegisterRequestNonAssistanceMutation(object):

    def __init__(self, register_request_non_assistance_use_case):
        self.register_request_non_assistance_use_case = register_request_non_assistance_use_case
    
    def set_params(self, trainer_user_identification, start_at, end_at, comment):
        self.trainer_user_identification = trainer_user_identification
        self.start_at = start_at
        self.end_at = end_at
        self.comment = comment
        return self
    
    def execute(self):
        return self.register_request_non_assistance_use_case.set_params(
            self.trainer_user_identification,
            self.start_at,
            self.end_at,
            self.comment
        ).execute()
    
class RegisterRequestNonAssistanceMutationFactory(object):

    @staticmethod
    def get():
        register_request_non_assistance_use_case = RegisterRequestNonAssistanceFactory.get()
        return RegisterRequestNonAssistanceMutation(register_request_non_assistance_use_case)


################################
# Change Status Request Non Assistance
################################

class ChangeStatusRequestNonAssistanceMutation(object):

    def __init__(self, change_status_request_non_assistance_use_case):
        self.change_status_request_non_assistance_use_case = change_status_request_non_assistance_use_case
    
    def set_params(self, request_non_assistance_id, status):
        self.request_non_assistance_id = request_non_assistance_id
        self.status = status
        return self
    
    def execute(self):
        return self.change_status_request_non_assistance_use_case.set_params(
            self.request_non_assistance_id,
            self.status
        ).execute()
    
class ChangeStatusRequestNonAssistanceMutationFactory(object):

    @staticmethod
    def get():
        change_status_request_non_assistance_use_case = ChangeStatusRequestNonAssistanceFactory.get()
        return ChangeStatusRequestNonAssistanceMutation(change_status_request_non_assistance_use_case)