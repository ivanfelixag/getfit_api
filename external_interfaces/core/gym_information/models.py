#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.base_user import BaseUserManager

class GymInformation(models.Model):
    title = models.CharField(_('Title'), max_length=60)
    description = models.TextField(_('Description'))
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)

    class Meta:
        verbose_name = _('Gym Information')
        verbose_name_plural = _('Gym Informations')