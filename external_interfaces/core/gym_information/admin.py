#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import GymInformation

admin.site.register(GymInformation)