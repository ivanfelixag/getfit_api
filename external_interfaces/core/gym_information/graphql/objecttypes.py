#!/usr/bin/env python
# -*- coding: utf-8 -*-

import graphene

class GymInformation(graphene.ObjectType):
    id = graphene.Int()
    title = graphene.String()
    description = graphene.String()
    created_at = graphene.DateTime()