#!/usr/bin/env python
# -*- coding: utf-8 -*-

from domain.entities.gym_information import GymInformation
from core.gym_information.models import GymInformation as ORMGymInformation

class GymInformationDatabaseRepo(object):

    def get_all_gym_informations(self):
        orm_gym_informations = ORMGymInformation.objects.all()
        return [self._decode_orm_gym_information(orm_gym_information) for orm_gym_information in orm_gym_informations]

    def _decode_orm_gym_information(self, orm_gym_information):
        return GymInformation(orm_gym_information.id, orm_gym_information.title, 
        orm_gym_information.description, orm_gym_information.created_at)

class GymInformationDatabaseRepoFactory(object):
    
    @staticmethod
    def get():
        return GymInformationDatabaseRepo()