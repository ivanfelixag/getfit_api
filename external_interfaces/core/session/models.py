#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from djongo import models as djongo_models
from django.utils.translation import ugettext_lazy as _
from ..user.models import User
from ..classes.models import Class

class Session(models.Model):
    MONDAY = 'MON'
    TUESDAY = 'TUES'
    WEDNESDAY = 'WED'
    THURSDAY = 'THURS'
    FRIDAY = 'FRI'
    SATURDAY = 'SAT'
    SUNDAY = 'SUN'
    DAY_CHOICES = (
        (MONDAY, 'MON'),
        (TUESDAY, 'TUES'),
        (WEDNESDAY, 'WED'),
        (THURSDAY, 'THURS'),
        (FRIDAY, 'FRI'),
        (SATURDAY, 'SAT'),
        (SUNDAY, 'DUN'),
    )
    day = models.CharField(_('Day'), choices=DAY_CHOICES, default=MONDAY, max_length=5)
    start_time = models.TimeField(verbose_name=_('Start time'))
    end_time = models.TimeField(verbose_name=_('End time'))
    capacity = models.IntegerField(_('Capacity'), default=1)
    trainer_user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, verbose_name=_('Trainer user'), related_name='trainer_user')

    class_session = models.ForeignKey(Class, on_delete=models.CASCADE, related_name="sessions",
        related_query_name="session", verbose_name=_('Session'))

    participants = models.ManyToManyField(User)

    class Meta:
        verbose_name = _('Session')
        verbose_name_plural = _('Sessions')

class HistoricalPeriodTrainerSession(djongo_models.Model):
    session_id = djongo_models.IntegerField()
    trainer_user_identification = djongo_models.CharField(max_length=30)
    start_date = djongo_models.DateField()
    end_date = djongo_models.DateField(null=True, blank=True)
    temporal_substitution = djongo_models.BooleanField(default=False)