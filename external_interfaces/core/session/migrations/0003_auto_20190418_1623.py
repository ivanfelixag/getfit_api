# Generated by Django 2.1 on 2019-04-18 16:23

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('session', '0002_auto_20190418_1120'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='session',
            name='trainer',
        ),
        migrations.AddField(
            model_name='session',
            name='trainer_user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='trainer_user', to=settings.AUTH_USER_MODEL, verbose_name='Trainer user'),
        ),
    ]
