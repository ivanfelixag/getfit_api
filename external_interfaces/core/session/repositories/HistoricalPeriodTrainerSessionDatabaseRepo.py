#!/usr/bin/env python
# -*- coding: utf-8 -*-

from domain.entities.historical_period_trainer_session import HistoricalPeriodTrainerSession
from core.session.models import HistoricalPeriodTrainerSession as ORMHistoricalPeriodTrainerSession

class HistoricalPeriodTrainerSessionDatabaseRepo(object):
    
    def register_historical_period(self, session_id, trainer_user_identification, start_date, end_date, temporal_substitution):
        orm_historical_period = ORMHistoricalPeriodTrainerSession.objects.create(
            session_id=session_id,
            trainer_user_identification=trainer_user_identification,
            start_date=start_date,
            end_date=end_date,
            temporal_substitution=temporal_substitution
        )

        return self._decode_orm_historical_period(orm_historical_period)

    
    def _decode_orm_historical_period(self, orm_historical_period):
        return HistoricalPeriodTrainerSession(
            orm_historical_period.id,
            orm_historical_period.session_id,
            orm_historical_period.trainer_user_identification,
            orm_historical_period.start_date,
            orm_historical_period.end_date,
            orm_historical_period.temporal_substitution
        )

class HistoricalPeriodTrainerSessionDatabaseRepoFactory(object):

    @staticmethod
    def get():
        return HistoricalPeriodTrainerSessionDatabaseRepo()