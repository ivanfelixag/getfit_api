#!/usr/bin/env python
# -*- coding: utf-8 -*-

from domain.entities.session import Session
from core.session.models import Session as ORMSession

class SessionDatabaseRepo(object):

    def get_session_by_id(self, id):
        orm_session = ORMSession.objects.get(id=id)
        return self._decode_orm_session(orm_session)
    
    def update_session_capacity(self, id, capacity):
        ORMSession.objects.filter(id=id).update(capacity=capacity) 

    def exists_session_by_id(self, id):
        return ORMSession.objects.filter(id=id).exists()

    def register_participant(self, id, user_identification):
        orm_session = ORMSession.objects.get(id=id)
        orm_session.participants.add(user_identification)

    def unsubscribe_participant(self, id, user_identification):
        orm_session = ORMSession.objects.get(id=id)
        orm_session.participants.remove(user_identification)

    def exists_participant_session(self, id, user_identification):
        orm_session = ORMSession.objects.get(id=id)
        return orm_session.participants.filter(pk=user_identification).exists()

    def register_session(self, day, start_time, end_time, capacity, trainer_user_identification, class_id):
        orm_session = ORMSession.objects.create(
            day=day,
            start_time=start_time,
            end_time=end_time,
            capacity=capacity,
            trainer_user_id=trainer_user_identification,
            class_session_id=class_id
        )
        return self._decode_orm_session(orm_session)   

    def _decode_orm_session(self, orm_session):
        return Session(orm_session.id, orm_session.day, orm_session.start_time, orm_session.end_time, orm_session.capacity, orm_session.trainer_user)

class SessionDatabaseRepoFactory(object):
    
    @staticmethod
    def get():
        return SessionDatabaseRepo()