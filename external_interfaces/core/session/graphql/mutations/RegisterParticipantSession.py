import graphene
from graphql_jwt.decorators import login_required
from core.user.graphql.objecttypes import User
from interface_adapters.mutations.session import RegisterParticipantSessionMutationFactory

class RegisterParticipantSession(graphene.Mutation):
    class Arguments:
        session_id = graphene.Int()
    
    session_id = graphene.Int()
    user_identification = graphene.String()

    @login_required
    def mutate(self, info, session_id):
        user_identification = info.context.user.identification
        mutation = RegisterParticipantSessionMutationFactory.get()
        mutation.set_params(session_id, user_identification).execute()
        return RegisterParticipantSession(session_id=session_id, user_identification=user_identification)