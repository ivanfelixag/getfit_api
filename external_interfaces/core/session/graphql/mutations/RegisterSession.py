import graphene
from graphql_jwt.decorators import login_required
from decorators.policies import user_has_role
from core.session.graphql.objecttypes import Session
from interface_adapters.mutations.session import RegisterSessionMutationFactory
from interface_adapters.repositories.user import UserRepo

class RegisterSession(graphene.Mutation):
    class Arguments:
        day = graphene.String()
        start_time = graphene.Time()
        end_time = graphene.Time()
        capacity = graphene.Int()
        trainer_user_identification = graphene.String()
        class_id = graphene.Int()
    
    session = graphene.Field(lambda: Session)

    @login_required
    @user_has_role(role=UserRepo.MANAGER_ROLE)
    def mutate(self, info, day, start_time, end_time, capacity, trainer_user_identification, class_id):
        mutation = RegisterSessionMutationFactory.get()
        session = mutation.set_params(day, start_time, end_time, capacity, trainer_user_identification, class_id).execute()
        return RegisterSession(session=session)