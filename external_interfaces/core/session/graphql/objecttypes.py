#!/usr/bin/env python
# -*- coding: utf-8 -*-

import graphene
from core.user.graphql.objecttypes import TrainerUser

class Session(graphene.ObjectType):
    id = graphene.Int()
    day = graphene.String()
    start_time = graphene.Time()
    end_time = graphene.Time()
    capacity = graphene.Int()
    trainer_user = graphene.Field(TrainerUser)