#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _

class Class(models.Model):
    description = models.CharField(_('Title'), max_length=255)
    active = models.BooleanField(_('Active'), default=True)
    class Meta:
        verbose_name = _('Class')
        verbose_name_plural = _('Classes')