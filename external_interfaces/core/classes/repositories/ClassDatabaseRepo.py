#!/usr/bin/env python
# -*- coding: utf-8 -*-

from domain.entities.classes import Class
from domain.entities.session import Session
from core.classes.models import Class as ORMClass

class ClassDatabaseRepo(object):

    def register_class(self, description, active):
        orm_class = ORMClass.objects.create(
            description=description,
            active=active
        )
        return self._decode_orm_class(orm_class)

    def exists_class_by_id(self, id):
        return ORMClass.objects.filter(id=id).exists()

    def get_all_classes(self):
        orm_classes = ORMClass.objects.prefetch_related('sessions','sessions__trainer_user').filter(active=True)
        return self._decode_orm_classes(orm_classes)

    def get_user_classes(self, user_identification):
        orm_classes = ORMClass.objects.prefetch_related('sessions','sessions__trainer_user').filter(active=True, session__participants__in=[user_identification]).distinct()
        classes = []
        for orm_class in orm_classes:
            orm_sessions = orm_class.sessions.filter(participants__in=[user_identification])
            sessions = [Session(orm_session.id, orm_session.day, orm_session.start_time, orm_session.end_time, orm_session.capacity, orm_session.trainer_user) for orm_session in orm_sessions]
            classes.append(Class(orm_class.id, orm_class.description, sessions, orm_class.active))
        return classes

    def get_trainer_user_classes(self, user_identification):
        orm_classes = ORMClass.objects.prefetch_related('sessions','sessions__trainer_user').filter(active=True, session__trainer_user=user_identification).distinct()
        classes = []
        for orm_class in orm_classes:
            orm_sessions = orm_class.sessions.filter(trainer_user=user_identification)
            sessions = [Session(orm_session.id, orm_session.day, orm_session.start_time, orm_session.end_time, orm_session.capacity, orm_session.trainer_user) for orm_session in orm_sessions]
            classes.append(Class(orm_class.id, orm_class.description, sessions, orm_class.active))
        return classes

    def _decode_orm_classes(self, orm_classes):
        return [self._decode_orm_class(orm_class) for orm_class in orm_classes]

    def _decode_orm_class(self, orm_class):
        sessions = orm_class.sessions.all()
        sessions = [Session(session.id, session.day, session.start_time, session.end_time, session.capacity, session.trainer_user) for session in sessions]
        return Class(orm_class.id, orm_class.description, sessions, orm_class.active)

class ClassDatabaseRepoFactory(object):
    
    @staticmethod
    def get():
        return ClassDatabaseRepo()