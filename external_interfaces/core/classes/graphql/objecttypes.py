#!/usr/bin/env python
# -*- coding: utf-8 -*-

import graphene
from core.session.graphql.objecttypes import Session

class Class(graphene.ObjectType):
    id = graphene.Int()
    description = graphene.String()
    sessions = graphene.List(Session)
    active = graphene.Boolean()