import graphene
from graphql_jwt.decorators import login_required
from decorators.policies import user_has_role
from core.classes.graphql.objecttypes import Class
from core.session.graphql.objecttypes import Session
from interface_adapters.mutations.classes import RegisterClassMutationFactory
from interface_adapters.repositories.user import UserRepo

class SessionInputType(graphene.InputObjectType):
    day = graphene.String(required=True)
    start_time = graphene.Time(required=True)
    end_time = graphene.Time(required=True)
    capacity = graphene.Int(required=True)
    trainer_user_identification = graphene.String(required=True)

class RegisterClass(graphene.Mutation):
    class Arguments:
        description = graphene.String()
        sessions = graphene.List(SessionInputType)
        active = graphene.Boolean()
    
    classs = graphene.Field(lambda: Class)

    @login_required
    @user_has_role(role=UserRepo.MANAGER_ROLE)
    def mutate(self, info, description, sessions, active):
        mutation = RegisterClassMutationFactory.get()
        classs = mutation.set_params(description, sessions, active).execute()
        return RegisterClass(classs=classs)