#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import User

admin.site.register(User)