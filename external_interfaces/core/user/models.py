#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.base_user import BaseUserManager

class Role(models.Model):
    name = models.CharField(_('Name'), max_length=60)

class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
    objects = UserManager()
    
    identification = models.CharField(_('Identification'), max_length=30, primary_key=True)
    first_name = models.CharField(_('First name'), max_length=60)
    last_name = models.CharField(_('Last name'), max_length=60)
    email = models.CharField(_('Email'), max_length=60)
    phone_number = models.CharField(_('Phone number'), max_length=30)
    active = models.BooleanField(_('Active'), default=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)

    roles = models.ManyToManyField(Role)

    USERNAME_FIELD = 'identification'

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')


class RequestNonAssistance(models.Model):

    trainer_user = models.ForeignKey(User, on_delete=models.CASCADE)
    start_at = models.DateField(_('Start at'))
    end_at = models.DateField(_('End at'))
    comment = models.CharField(_('Comment'), max_length=255)
    status = models.CharField(_('Status'), max_length=8)