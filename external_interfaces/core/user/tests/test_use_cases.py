#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.test import TestCase

class TestUseCases(TestCase):
    identification = "11111111A"

    def test_1_create_user(self):
        user = self._create_user()
        self.assertEqual(user.identification, self.identification)
    
    def test_2_check_duplicated_identification(self):
        self._create_user()

        try:
            self._create_user()
            self.fail("Duplicated identification - User has been created")
        except Exception:
            pass

    def _create_user(self):
        from domain.use_cases.user.CreateUser import CreateUserFactory

        first_name = "User first name"
        last_name = "User last name"
        email = "user@email.com"
        identification = self.identification
        password = "pa55word"
        phone_number = "+34 111 11 11 11"

        create_user = CreateUserFactory.get()
        user = create_user.set_params(first_name, last_name, email, identification, password, phone_number).execute()
        return user