#!/usr/bin/env python
# -*- coding: utf-8 -*-

from domain.entities.user import User
from domain.entities.role import Role
from core.user.models import User as ORMUser

class UserDatabaseRepo(object):

    def create_user(self, first_name, last_name, email, identification, password, phone_number):
        orm_user = ORMUser.objects.create(
            first_name=first_name,
            last_name=last_name,
            email=email,
            identification=identification,
            phone_number=phone_number
        )
        orm_user.set_password(password)
        orm_user.save()
        return self._decode_orm_user(orm_user)
    
    def disable_user(self, identification):
        ORMUser.objects.filter(identification=identification).update(active=False)

    def get_user_by_identification(self, identification):
        orm_user = ORMUser.objects.prefetch_related("roles").get(identification=identification, active=True)
        return self._decode_orm_user(orm_user)

    def exists_user_identification(self, identification, check_active=True):
        if check_active:
            return ORMUser.objects.filter(identification=identification, active=True).exists()
        else:
            return ORMUser.objects.filter(identification=identification).exists()

    def get_trainers_users(self, active):
        orm_users = ORMUser.objects.prefetch_related("roles").filter(roles__name="trainer", active=active)
        return self._decode_orm_users(orm_users)
    
    def user_has_role(self, identification, role):
        return ORMUser.objects.prefetch_related("roles").filter(identification=identification, roles__name=role).exists()

    def _decode_orm_user(self, orm_user):
        roles = orm_user.roles.all()
        roles = [Role(role.id, role.name) for role in roles]
        return User(orm_user.first_name, orm_user.last_name, orm_user.email, orm_user.identification, orm_user.password, orm_user.phone_number, orm_user.active, roles)

    def _decode_orm_users(self, orm_users):
        return [self._decode_orm_user(orm_user) for orm_user in orm_users]

class UserDatabaseRepoFactory(object):
    
    @staticmethod
    def get():
        return UserDatabaseRepo()