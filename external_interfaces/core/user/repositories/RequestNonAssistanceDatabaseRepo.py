#!/usr/bin/env python
# -*- coding: utf-8 -*-

from domain.entities.request_non_assistance import RequestNonAssistance
from core.user.models import RequestNonAssistance as ORMRequestNonAssistance

class RequestNonAssistanceDatabaseRepo(object):

    def register_request_non_assistance(self, trainer_user_id, start_at, end_at, comment, status):
        orm_request_non_assistance = ORMRequestNonAssistance.objects.create(
            trainer_user_id=trainer_user_id,
            start_at=start_at,
            end_at=end_at,
            comment=comment,
            status=status
        )
        return self._decode_orm_request_non_assistance(orm_request_non_assistance)

    def get_request_non_assistance_by_id(self, id):
        orm_request_non_assistance = ORMRequestNonAssistance.objects.get(id=id)
        return self._decode_orm_request_non_assistance(orm_request_non_assistance)

    def get_requests_non_assistance(self, status):
        orm_requests_non_assistance = ORMRequestNonAssistance.objects.filter(status=status)
        return self._decode_orm_requests_non_assistance(orm_requests_non_assistance)

    def update_status_request_non_assistance(self, id, status):
        ORMRequestNonAssistance.objects.filter(id=id).update(status=status)

    def _decode_orm_request_non_assistance(self, orm_request_non_assistance):
        return RequestNonAssistance(
            orm_request_non_assistance.id,
            orm_request_non_assistance.trainer_user_id,
            orm_request_non_assistance.start_at,
            orm_request_non_assistance.end_at,
            orm_request_non_assistance.comment,
            orm_request_non_assistance.status
        )
    
    def _decode_orm_requests_non_assistance(self, orm_requests_non_assistance):
        return [self._decode_orm_request_non_assistance(orm_request_non_assistance) for orm_request_non_assistance in orm_requests_non_assistance]

class RequestNonAssistanceDatabaseRepoFactory(object):
    
    @staticmethod
    def get():
        return RequestNonAssistanceDatabaseRepo()