import graphene
from graphql_jwt.decorators import login_required
from interface_adapters.mutations.user import UnsubscribeUserMutationFactory

class UnsubscribeUser(graphene.Mutation):
    identification = graphene.String()

    @login_required
    def mutate(self, info, **kwargs):
        identification = info.context.user.identification
        mutation = UnsubscribeUserMutationFactory.get()
        mutation.set_params(identification).execute()
        return UnsubscribeUser(identification=identification)