import graphene
from core.user.graphql.objecttypes import User
from interface_adapters.mutations.user import RegisterUserMutationFactory

class RegisterUser(graphene.Mutation):
    class Arguments:
        first_name = graphene.String()
        last_name = graphene.String()
        email = graphene.String()
        identification = graphene.String()
        password = graphene.String()
        phone_number = graphene.String()

    user = graphene.Field(lambda: User)

    def mutate(self, info, first_name, last_name, email, identification, password, phone_number):
        mutation = RegisterUserMutationFactory.get()
        user = mutation.set_params(first_name, last_name, email, identification, password, phone_number).execute()
        return RegisterUser(user=user)