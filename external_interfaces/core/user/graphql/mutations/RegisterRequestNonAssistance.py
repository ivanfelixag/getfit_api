import graphene
from graphql_jwt.decorators import login_required
from decorators.policies import user_has_role
from core.user.graphql.objecttypes import RequestNonAssistance
from interface_adapters.mutations.user import RegisterRequestNonAssistanceMutationFactory
from interface_adapters.repositories.user import UserRepo

class RegisterRequestNonAssistance(graphene.Mutation):
    class Arguments:
        start_at = graphene.Date()
        end_at = graphene.Date()
        comment = graphene.String()

    request_non_assistance = graphene.Field(lambda: RequestNonAssistance)

    @login_required
    @user_has_role(role=UserRepo.TRAINER_ROLE)
    def mutate(self, info, start_at, end_at, comment):
        mutation = RegisterRequestNonAssistanceMutationFactory.get()
        request_non_assistance = mutation.set_params(
            info.context.user.identification,
            start_at,
            end_at,
            comment
        ).execute()
        return RegisterRequestNonAssistance(request_non_assistance=request_non_assistance)