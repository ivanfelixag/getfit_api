import graphene
from graphql_jwt.decorators import login_required
from decorators.policies import user_has_role
from core.user.graphql.objecttypes import RequestNonAssistance
from interface_adapters.mutations.user import ChangeStatusRequestNonAssistanceMutationFactory
from interface_adapters.repositories.user import UserRepo

class ChangeStatusRequestNonAssistance(graphene.Mutation):
    class Arguments:
        id = graphene.Int()
        status = graphene.String()

    request_non_assistance = graphene.Field(lambda: RequestNonAssistance)

    @login_required
    @user_has_role(role=UserRepo.MANAGER_ROLE)
    def mutate(self, info, id, status):
        mutation = ChangeStatusRequestNonAssistanceMutationFactory.get()
        request_non_assistance = mutation.set_params(
            id,
            status
        ).execute()
        return ChangeStatusRequestNonAssistance(request_non_assistance=request_non_assistance)