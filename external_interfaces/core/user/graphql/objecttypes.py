#!/usr/bin/env python
# -*- coding: utf-8 -*-

import graphene

class UserRole(graphene.ObjectType):
    id = graphene.Int()
    name = graphene.String()

class User(graphene.ObjectType):
    first_name = graphene.String()
    last_name = graphene.String()
    email = graphene.String()
    identification = graphene.String()
    phone_number = graphene.String()
    active = graphene.Boolean()
    roles = graphene.List(UserRole)

class TrainerUser(graphene.ObjectType):
    first_name = graphene.String()
    last_name = graphene.String()
    active = graphene.Boolean()

class RequestNonAssistance(graphene.ObjectType):
    trainer_user_identification = graphene.String()
    start_at = graphene.Date()
    end_at = graphene.Date()
    comment = graphene.String()
    status = graphene.String()