#!/usr/bin/env python
# -*- coding: utf-8 -*-
import graphene
import graphql_jwt
from graphql_jwt.decorators import login_required
from decorators.policies import user_has_role

from core.classes.graphql.objecttypes import Class
from core.gym_information.graphql.objecttypes import GymInformation
from core.user.graphql.objecttypes import User, TrainerUser, RequestNonAssistance

from core.user.graphql.mutations.RegisterUser import RegisterUser
from core.user.graphql.mutations.UnsubscribeUser import UnsubscribeUser
from core.user.graphql.mutations.RegisterRequestNonAssistance import RegisterRequestNonAssistance
from core.user.graphql.mutations.ChangeStatusRequestNonAssistance import ChangeStatusRequestNonAssistance
from core.session.graphql.mutations.RegisterSession import RegisterSession
from core.classes.graphql.mutations.RegisterClass import RegisterClass
from core.session.graphql.mutations.RegisterParticipantSession import RegisterParticipantSession
from core.session.graphql.mutations.UnsubscribeParticipantSession import UnsubscribeParticipantSession

from interface_adapters.repositories.user import UserRepo

from interface_adapters.resolvers.user import UserResolverFactory, TrainersUsersResolverFactory, RequestsNonAssistanceResolverFactory
from interface_adapters.resolvers.gym_information import GymInformationResolverFactory
from interface_adapters.resolvers.classes import ClassesResolverFactory, UserClassesResolverFactory, TrainerUserClassesResolverFactory

class Query(graphene.ObjectType):
    user = graphene.Field(User, identification=graphene.String())
    trainers_users = graphene.List(TrainerUser, active=graphene.Boolean())
    gym_information = graphene.List(GymInformation)
    classes = graphene.List(Class)
    user_classes = graphene.List(Class)
    trainer_user_classes = graphene.List(Class)
    requests_non_assistance = graphene.List(RequestNonAssistance, status=graphene.String())

    @login_required
    def resolve_user(self, info, **kwargs):
        user_identification = info.context.user.identification
        resolver = UserResolverFactory.get()
        return resolver.set_params(user_identification).execute()

    def resolve_trainers_users(self, info, active):
        resolver = TrainersUsersResolverFactory.get()
        return resolver.set_params(active).execute()

    def resolve_gym_information(self, info):
        resolver = GymInformationResolverFactory.get()
        return resolver.execute()

    @login_required
    def resolve_classes(self, info, **kwargs):
        resolver = ClassesResolverFactory.get()
        return resolver.execute()
    
    @login_required
    def resolve_user_classes(self, info, **kwargs):
        user_identification = info.context.user.identification
        resolver = UserClassesResolverFactory.get()
        return resolver.set_params(user_identification).execute()

    @login_required
    def resolve_trainer_user_classes(self, info, **kwargs):
        user_identification = info.context.user.identification
        resolver = TrainerUserClassesResolverFactory.get()
        return resolver.set_params(user_identification).execute()
    
    @login_required
    @user_has_role(role=UserRepo.MANAGER_ROLE)
    def resolve_requests_non_assistance(self, info, status):
        resolver = RequestsNonAssistanceResolverFactory.get()
        return resolver.set_params(status).execute()

class Mutations(graphene.ObjectType):
    # User
    register_user = RegisterUser.Field()
    unsubscribe_user = UnsubscribeUser.Field()
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()

    # Trainer
    register_request_non_assistance = RegisterRequestNonAssistance.Field()
    change_status_request_non_assistance = ChangeStatusRequestNonAssistance.Field()

    # Session
    register_session = RegisterSession.Field()
    register_participant_session = RegisterParticipantSession.Field()
    unsubscribe_participant_session = UnsubscribeParticipantSession.Field()

    # Class
    register_class = RegisterClass.Field()

schema = graphene.Schema(query=Query, mutation=Mutations)