#!/usr/bin/env python
# -*- coding: utf-8 -*-
from common.interfaces.IMailingStrategy import IMailingStrategy

class MailingService:
    class Recipient:
        def __init__(self, name, email):
            self.name = name
            self.email = email

    def __init__(self, strategy):
        self._strategy = strategy

    def execute(self):
        self._strategy.send()


class NewUserMailingStrategy(IMailingStrategy):
    MAIL_ID = ""

    def mail_id(self):
        return self.MAIL_ID

    def set_params(self, recipient):
        self.recipient = recipient
        return self

    def send(self):
        pass

class DeleteClassMailingStrategy(IMailingStrategy):
    MAIL_ID = ""

    def mail_id(self):
        return self.MAIL_ID

    def set_params(self, recipients, class_name):
        self.recipients = recipients
        self.class_name = class_name
        return self

    def send(self):
        pass


class PermanentChangeTimeSessionMailingStrategy(IMailingStrategy):
    MAIL_ID = ""

    def mail_id(self):
        return self.MAIL_ID

    def set_params(self, recipients, session_day, session_start_time, 
            session_end_time, class_name):
        self.recipients = recipients
        self.session_day = session_day
        self.session_start_time = session_start_time
        self.session_end_time = session_end_time
        self.class_name = class_name
        return self

    def send(self):
        pass


class DeleteSessionMailingStrategy(IMailingStrategy):
    MAIL_ID = ""

    def mail_id(self):
        return self.MAIL_ID

    def set_params(self, recipients, session_day, session_start_time, 
            session_end_time, class_name):
        self.recipients = recipients
        self.session_day = session_day
        self.session_start_time = session_start_time
        self.session_end_time = session_end_time
        self.class_name = class_name
        return self

    def send(self):
        pass


class RequestNonAssistanceMailingStrategy(IMailingStrategy):
    MAIL_ID = ""

    def mail_id(self):
        return self.MAIL_ID

    def set_params(self, recipients, trainer_user_full_name,
        trainer_user_email, trainer_user_identification):
        self.recipients = recipients
        self.trainer_user_full_name = trainer_user_full_name
        self.trainer_user_email = trainer_user_email
        self.trainer_user_identification = trainer_user_identification
        return self

    def send(self):
        pass

class ReportRequestNonAssistanceApprovedMailingStrategy(IMailingStrategy):
    MAIL_ID = ""

    def mail_id(self):
        return self.MAIL_ID

    def set_params(self, recipient):
        self.recipient = recipient
        return self

    def send(self):
        pass


class ReportRequestNonAssistanceDeniedMailingStrategy(IMailingStrategy):
    MAIL_ID = ""

    def mail_id(self):
        return self.MAIL_ID

    def set_params(self, recipient):
        self.recipient = recipient
        return self

    def send(self):
        pass


class NewTrainerContractMailingStrategy(IMailingStrategy):
    MAIL_ID = ""

    def mail_id(self):
        return self.MAIL_ID

    def set_params(self, recipient):
        self.recipient = recipient
        return self

    def send(self):
        pass