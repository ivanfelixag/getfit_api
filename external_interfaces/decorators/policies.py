#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .utils.passes_test import user_passes_test
from interface_adapters.decorators.policies import UserHasRoleDecoratorFactory

def user_has_role(role):
    def check_role(user):
        user_has_role_decorator = UserHasRoleDecoratorFactory.get()
        user_has_role_decorator.set_params(user.identification, role).execute()
        return True
    return user_passes_test(check_role)