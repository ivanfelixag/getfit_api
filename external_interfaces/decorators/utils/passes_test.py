#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import wraps
from django.utils.translation import ugettext_lazy as _

class GraphQLError(Exception):
    default_message = _('A server error occurred')
    default_code = 'error'

    def __init__(self, message=None, code=None, **data):
        if message is None:
            message = self.default_message

        if code is None:
            code = self.default_code

        self.code = code
        self.error_data = data

        super().__init__(message)

class PermissionDenied(GraphQLError):
    default_message = _('You do not have permission to perform this action')
    default_code = 'permissionDenied'

def context(f):
    def decorator(func):
        def wrapper(*args, **kwargs):
            info = args[f.__code__.co_varnames.index('info')]
            return func(info.context, *args, **kwargs)
        return wrapper
    return decorator


def user_passes_test(test_func):

    def decorator(f):
        @wraps(f)
        @context(f)
        def wrapper(context, *args, **kwargs):
            if test_func(context.user):
                return f(*args, **kwargs)
            raise PermissionDenied()
        return wrapper
    return decorator