django==2.1
mysqlclient==1.3.13
graphene-django==2.2.0
django-nose==1.4.6
coverage==4.5.3
django-graphql-jwt==0.2.1
python-redis-lock[django]
djongo