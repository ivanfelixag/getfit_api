#!/usr/bin/env python
# -*- coding: utf-8 -*-

from core.session.models import HistoricalPeriodTrainerSession as ORMHistoricalPeriodTrainerSession

class GetFitApiRouter(object):

    def db_for_read(self, model, **hints):
        if model in [ORMHistoricalPeriodTrainerSession]:
            return 'historical_db'
        return None

    def db_for_write(self, model, **hints):
        if model in [ORMHistoricalPeriodTrainerSession]:
            return 'historical_db'
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if model_name in ['historicalperiodtrainersession']:
            return db == 'historical_db'
        else:
            return db == 'default'