#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import redis_lock
from redis import StrictRedis
from common.interfaces.ILock import ILock
from django.conf import settings

class LockManager(ILock):

    connection = None
    
    @classmethod
    def begin_lock(cls, name):
        if not cls.connection:
            cls.connection = StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT)
        return redis_lock.Lock(cls.connection, name=name, expire=20, auto_renewal=True)