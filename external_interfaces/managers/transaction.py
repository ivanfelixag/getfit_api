#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import transaction
from common.interfaces.ITransaction import ITransaction

class TransactionManager(ITransaction):

    @classmethod
    def begin_transaction(cls):
        if not hasattr(transaction, 'inner_transactions'):
            transaction.inner_transactions = 0
            transaction.set_autocommit(False)
        else:
            transaction.inner_transactions += 1

    @classmethod
    def do_roolback(cls):
        if transaction.inner_transactions == 0:
            transaction.rollback()
    
    @classmethod
    def do_commit(cls):
        if transaction.inner_transactions == 0:
            transaction.commit()
    
    @classmethod
    def end_transaction(cls):
        if transaction.inner_transactions == 0:
            del transaction.inner_transactions
            transaction.set_autocommit(True)
        else:
            transaction.inner_transactions -= 1