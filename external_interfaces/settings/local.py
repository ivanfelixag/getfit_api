#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'getfit',
        'USER': 'root',
        'PASSWORD': 'getfitPa55#',
        'HOST': '127.0.0.1',
        'PORT': 3306,
    },
    'historical_db': {
        'ENGINE': 'djongo',
        'NAME': 'getfit',
        'USER': 'root',
        'PASSWORD': 'getfitPa55#',
        'HOST': '127.0.0.1',
        'PORT': 27017,
    }
}

REDIS_HOST = '127.0.0.1'
REDIS_PORT = '6379'