#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .base import *

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get('DATABASE_NAME', ''),
        'USER': 'root',
        'PASSWORD': os.environ.get('DATABASE_PASSWORD', ''),
        'HOST': os.environ.get('DATABASE_HOSTNAME', ''),
        'PORT': os.environ.get('DATABASE_PORT', 3306),
    },
    'historical_db': {
        'ENGINE': 'djongo',
        'NAME': os.environ.get('MONGODB_NAME', ''),
        'USER': os.environ.get('MONGODB_USER', ''),
        'PASSWORD': os.environ.get('MONGODB_PASSWORD', ''),
        'HOST': os.environ.get('MONGODB_HOST', ''),
        'PORT': os.environ.get('MONGODB_PORT', ''),
    }
}

REDIS_HOST = os.environ.get('REDIS_HOST', '')
REDIS_PORT = os.environ.get('REDIS_PORT', '')