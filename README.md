
# What is this project?
This project is a fusion between the [Django framework](https://www.djangoproject.com/), [Graphene-Django](https://github.com/graphql-python/graphene-django) and the [Uncle Bob's concept of clean architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html).

The main objective is to understand and show how to create a project with an architecture that separates my business logic and the Django framework.

## Folder distribution:

````
common: Folder with all the exceptions that the code can throw and the interfaces for the managers and services implementation.

docs: Small pieces of documentation that describe modifications or my particular vision of how to apply certain concepts 
      in a clean architecture.

domain: All the content related with our domain layer.
    entities: Our entities layer.
    use_cases: Our business logic - Use cases layer.
    validators: Folder with the validators that the use cases use.

external_interfaces: Input layer with GraphQL (graphene), all Django framework files, database connection, managers, 
                     services... that connect to the outside.

interface_adapters: Code pieces that act as interface, helping to uncouple the application layer with the outermost layer.

server: The server configuration for both the local and production environment.
````

## Project Description

This project consists of an API that allows interactions between users, trainers, managers and classes

### Use cases:

#### Users
- [DONE] Get token
- [DONE] Register user
    - Notify the user
- [DONE] Get user
- [DONE] Get gym information
- [DONE] Get trainers users
- [DONE] Get gym classes
- [DONE] Register a new participant of the session (Pending tests - Create session use case)
- [DONE] Get user classes
- [DONE] Unsubscribe a participant of the session
- [DONE] Unsubscribe user

#### Trainers
- [DONE] Get token
- [DONE] Get trainer user classes (See calendar of assigned classes)
- [DONE] Request for non-assistance
    - [DONE] Establish the range of non-attendance of the trainers
    - [DONE] Store the request for a record
    - Notify the managers

#### Managers
- [DONE] Get token
- [DONE] List the requests for non-attendance of the trainers (status values: pending, approved or denied)
    - [DONE] Approve the request of not attending a trainer (Change request status)
        - Notify the trainer
    - [DONE] Denying a trainer's request for non-attendance (Change request status)
        - Notify the trainer
- [DONE] List of active trainers
- [DONE] List of past trainers
- Create a contract for a trainer
    - Notify the trainer that he will soon be able to manage sessions
- Finalize trainer contract (Validate the trainer hasn't got any session)
- See the profile of a trainer
    - Show the history of classes that the trainer has managed
    - Show the classes that are currently managed
- [DONE] Calendar with the classes created from the gym - Get gym classes
- [DONE] Register a new class
    - [DONE] Specify the limit of places
    - [DONE] Assign schedule (days and hours) to the class
    - [DONE] Assign trainer to the class
        - [DONE] Register an historical period for the trainer for each session of the class
- See detail of a class
    - Show the history of trainers who have managed the class with the average scores of the members who have participated in it
    - [DONE] Show the class schedule (days and hours)
    - Change session trainer
        - Indefinitely
            - Update the end date field for the old trainer historical period
            - Register an historical period for the new trainer of the session
        - Temporarily (set max date) (CRON)
            - Register an historical period for the new trainer for the session indicating that is temporal substitution
    - Schedule the deactivation of the class (Days in advance to notify, the reason why the class is deactivated and 
      boolean to deactivate or not the class directly without the need to schedule it)
        - Deactivate and Notify members (all class sessions | CRON)
        - Update the end date field for the trainers historical periods
    - [DONE] Register a new session
    - Schedule a date for the permanent schedule change of a session
        - Apply schedule change and Notify members (CRON)
    - Schedule delete (soft delete) a session
        - Delete and Notify members (CRON)
            - Update the end date field for the trainer historical period

#### Platform Administrators
- Register Managers
    - Set working hours in the week (days and hours)
- Unsubscribe Managers


# Preconfiguration of the environments
- Install docker: https://docs.docker.com/install/
- Install docker-compose: https://docs.docker.com/compose/install/

# Configuration of the development environment
- Create the virtual environment in the root folder
```bash
virtualenv -p python3 venv
```
- Install the requirements
```bash
pip3 install -r external_interfaces/requirements.txt
```
- Run the database with docker
```bash
docker-compose -f server/docker/local/docker-compose.yml down
docker-compose -f server/docker/local/docker-compose.yml build
docker-compose -f server/docker/local/docker-compose.yml up -d
```
- [JUST ONCE] Create platform administrator
```bash
python3 external_interfaces/manage.py createsuperuser
```

- [OPTIONAL] Create migrations
````
python3 external_interfaces/manage.py makemigrations
python3 external_interfaces/manage.py migrate
python3 external_interfaces/manage.py migrate --database=historical_db
````

- Run the Django project
```bash
python3 external_interfaces/manage.py runserver
```

- Run the test cases
```bash
python3 external_interfaces/manage.py test
```

- [OPEN IN BROWSER] GraphQL explorer: http://127.0.0.1:8000/explore
- [OPEN IN BROWSER] Adminer: http://127.0.0.1:8080
- API GraphQL: http://127.0.0.1:8000/graphql

# Configuration of the production environment
- Run with docker
```bash
docker-compose -f server/docker/prod/docker-compose.yml down
docker-compose -f server/docker/prod/docker-compose.yml build
docker-compose -f server/docker/prod/docker-compose.yml up -d
```
- [JUST ONCE] Create platform administrator
```bash
docker exec -it getfit_api bash
python3 external_interfaces/manage.py createsuperuser
```

# More information

For more information see the [docs folder](docs)