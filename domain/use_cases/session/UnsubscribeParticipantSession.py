#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ..BaseUseCaseObject import BaseUseCaseObject
from interface_adapters.repositories.session import SessionRepoFactory
from interface_adapters.managers.transaction import TransactionManager
from interface_adapters.managers.lock import LockManager
from domain.validators.user.ExistsUserIdentificationValidator import ExistsUserIdentificationValidatorFactory
from domain.validators.session.ExistsSessionIdValidator import ExistsSessionIdValidatorFactory
from domain.validators.session.ExistsParticipantSessionValidator import ExistsParticipantSessionValidatorFactory 

class UnsubscribeParticipantSession(BaseUseCaseObject):

    def __init__(self, session_repo, exists_user_identification_validator, 
        exists_session_id_validator, exists_participant_session_validator):
        self.session_repo = session_repo
        self.exists_user_identification_validator = exists_user_identification_validator
        self.exists_session_id_validator = exists_session_id_validator
        self.exists_participant_session_validator = exists_participant_session_validator

    def set_params(self, session_id, user_identification):
        self.session_id = session_id
        self.user_identification = user_identification
        return self

    def execute(self):
        self.exists_user_identification_validator.set_params(self.user_identification).execute()
        self.exists_session_id_validator.set_params(self.session_id).execute()
        self.exists_participant_session_validator.set_params(self.session_id, self.user_identification).execute()

        with LockManager.begin_lock(f"LOCK_PARTICIPANT_SESSION_{self.session_id}"):
            session = self.session_repo.get_session_by_id(self.session_id)

            TransactionManager.begin_transaction()
            try:
                self.session_repo.update_session_capacity(session.id, session.capacity + 1)
                self.session_repo.unsubscribe_participant(session.id, self.user_identification)
            except Exception as ex:
                TransactionManager.do_roolback()
                raise ex
            else:
                TransactionManager.do_commit()
            finally:
                TransactionManager.end_transaction()


class UnsubscribeParticipantSessionFactory(object):

    @staticmethod
    def get():
        session_repo = SessionRepoFactory.get()
        exists_user_identification_validator = ExistsUserIdentificationValidatorFactory.get()
        exists_session_id_validator = ExistsSessionIdValidatorFactory.get()
        exists_participant_session_validator = ExistsParticipantSessionValidatorFactory.get()
        return UnsubscribeParticipantSession(session_repo, exists_user_identification_validator, 
            exists_session_id_validator, exists_participant_session_validator)