#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ..BaseUseCaseObject import BaseUseCaseObject
from interface_adapters.managers.transaction import TransactionManager
from interface_adapters.repositories.session import SessionRepoFactory
from interface_adapters.repositories.historical_period_trainer_session import HistoricalPeriodTrainerSessionRepoFactory
from domain.validators.classes.ExistsClassIdValidator import ExistsClassIdValidatorFactory
from domain.validators.session.ExistsSessionDayValidator import ExistsSessionDayValidatorFactory
from domain.validators.session.SessionRangeHoursValidator import SessionRangeHoursValidatorFactory
from domain.validators.user.ExistsUserIdentificationValidator import ExistsUserIdentificationValidatorFactory
from domain.validators.user.UserIsTrainerValidator import UserIsTrainerValidatorFactory

class RegisterSession(BaseUseCaseObject):

    def __init__(self, session_repo, historical_period_trainer_session_repo,
            exists_class_id_validator, exists_session_day_validator, 
            session_range_hours_validator, exists_user_identification_validator, user_is_trainer_validator):
        self.session_repo = session_repo
        self.historical_period_trainer_session_repo = historical_period_trainer_session_repo
        self.exists_class_id_validator = exists_class_id_validator
        self.exists_session_day_validator = exists_session_day_validator
        self.session_range_hours_validator = session_range_hours_validator
        self.exists_user_identification_validator = exists_user_identification_validator
        self.user_is_trainer_validator = user_is_trainer_validator

    def set_params(self, day, start_time, end_time, capacity, trainer_user_identification, class_id):
        self.day = day
        self.start_time = start_time
        self.end_time = end_time
        self.capacity = capacity
        self.trainer_user_identification = trainer_user_identification
        self.class_id = class_id
        return self

    def execute(self):
        self.exists_class_id_validator.set_params(self.class_id).execute()
        self.exists_session_day_validator.set_params(self.day).execute()
        self.session_range_hours_validator.set_params(self.start_time, self.end_time).execute()
        self.exists_user_identification_validator.set_params(self.trainer_user_identification).execute()
        self.user_is_trainer_validator.set_params(self.trainer_user_identification).execute()
        
        TransactionManager.begin_transaction()
        try:
            session = self.session_repo.register_session(self.day, self.start_time, self.end_time, 
                    self.capacity, self.trainer_user_identification, self.class_id)

            self.historical_period_trainer_session_repo.register_historical_period(
                session.id,
                self.trainer_user_identification
            )

            return session
        except Exception as ex:
            TransactionManager.do_roolback()
            raise ex
        else:
            TransactionManager.do_commit()
        finally:
            TransactionManager.end_transaction()


class RegisterSessionFactory(object):

    @staticmethod
    def get():
        session_repo = SessionRepoFactory.get()
        historical_period_trainer_session_repo = HistoricalPeriodTrainerSessionRepoFactory.get()
        exists_class_id_validator = ExistsClassIdValidatorFactory.get()
        exists_session_day_validator = ExistsSessionDayValidatorFactory.get()
        session_range_hours_validator = SessionRangeHoursValidatorFactory.get()
        exists_user_identification_validator = ExistsUserIdentificationValidatorFactory.get()
        user_is_trainer_validator = UserIsTrainerValidatorFactory.get()
        return RegisterSession(session_repo, historical_period_trainer_session_repo, 
                    exists_class_id_validator, exists_session_day_validator, 
                    session_range_hours_validator, exists_user_identification_validator, user_is_trainer_validator)