#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ..BaseUseCaseObject import BaseUseCaseObject
from interface_adapters.repositories.gym_information import GymInformationRepoFactory

class GetGymInformation(BaseUseCaseObject):

    def __init__(self, gym_information_repo):
        self.gym_information_repo = gym_information_repo

    def execute(self):
        return self.gym_information_repo.get_all_gym_informations()


class GetGymInformationFactory(object):

    @staticmethod
    def get():
        gym_information_repo = GymInformationRepoFactory.get()
        return GetGymInformation(gym_information_repo)