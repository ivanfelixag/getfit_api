#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseUseCaseObject import BaseUseCaseObject
from domain.validators.user.NotExistsUserIdentificationValidator import NotExistsUserIdentificationValidatorFactory
from interface_adapters.repositories.user import UserRepoFactory

class RegisterUser(BaseUseCaseObject):
    
    def __init__(self, user_repo, not_exists_user_identification_validator):
        self.user_repo = user_repo
        self.not_exists_user_identification_validator = not_exists_user_identification_validator

    def set_params(self, first_name, last_name, email, identification, password, phone_number):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.identification = identification
        self.password = password
        self.phone_number = phone_number
        return self
    
    def execute(self):
        self.not_exists_user_identification_validator.set_params(self.identification, check_active=False).execute()
        return self.user_repo.create_user(self.first_name, self.last_name, self.email, self.identification, self.password, self.phone_number)

class RegisterUserFactory(object):

    @staticmethod
    def get():
        user_repo = UserRepoFactory.get()
        not_exists_user_identification_validator = NotExistsUserIdentificationValidatorFactory.get()
        return RegisterUser(user_repo, not_exists_user_identification_validator)