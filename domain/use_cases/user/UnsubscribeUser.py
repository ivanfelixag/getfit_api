#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseUseCaseObject import BaseUseCaseObject
from interface_adapters.repositories.user import UserRepoFactory
from interface_adapters.managers.transaction import TransactionManager
from domain.validators.user.ExistsUserIdentificationValidator import ExistsUserIdentificationValidatorFactory
from domain.use_cases.classes.GetUserClasses import GetUserClassesFactory
from domain.use_cases.session.UnsubscribeParticipantSession import UnsubscribeParticipantSessionFactory

class UnsubscribeUser(BaseUseCaseObject):

    def __init__(self, user_repo, exists_user_identification_validator, get_user_classes_use_case, unsubscribe_participant_session_use_case):
        self.user_repo = user_repo
        self.exists_user_identification_validator = exists_user_identification_validator
        self.get_user_classes_use_case = get_user_classes_use_case
        self.unsubscribe_participant_session_use_case = unsubscribe_participant_session_use_case

    def set_params(self, identification):
        self.identification = identification
        return self
    
    def execute(self):
        self.exists_user_identification_validator.set_params(self.identification).execute()

        TransactionManager.begin_transaction()
        try:
            user_classes = self.get_user_classes_use_case.set_params(self.identification).execute()
            for user_class in user_classes:
                for session in user_class.sessions:
                    self.unsubscribe_participant_session_use_case.set_params(session.id, self.identification).execute()
            self.user_repo.disable_user(self.identification)
        except Exception as ex:
            TransactionManager.do_roolback()
            raise ex
        else:
            TransactionManager.do_commit()
        finally:
            TransactionManager.end_transaction()


class UnsubscribeUserFactory(object):

    @staticmethod
    def get():
        user_repo = UserRepoFactory.get()
        exists_user_identification_validator = ExistsUserIdentificationValidatorFactory.get()
        get_user_classes_use_case = GetUserClassesFactory.get()
        unsubscribe_participant_session_use_case = UnsubscribeParticipantSessionFactory.get()
        return UnsubscribeUser(user_repo, exists_user_identification_validator, 
                get_user_classes_use_case, unsubscribe_participant_session_use_case)