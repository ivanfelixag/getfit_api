#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseUseCaseObject import BaseUseCaseObject
from interface_adapters.repositories.request_non_assistance import RequestNonAssistanceRepoFactory

class GetRequestsNonAssistance(BaseUseCaseObject):
    
    def __init__(self, request_non_assistance_repo):
        self.request_non_assistance_repo = request_non_assistance_repo
    
    def set_params(self, status='pending'):
        self.status = status
        return self

    def execute(self):
        return self.request_non_assistance_repo.get_requests_non_assistance(self.status)


class GetRequestsNonAssistanceFactory(object):

    @staticmethod
    def get():
        request_non_assistance_repo = RequestNonAssistanceRepoFactory.get()
        return GetRequestsNonAssistance(request_non_assistance_repo)