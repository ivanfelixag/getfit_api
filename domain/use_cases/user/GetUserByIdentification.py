#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseUseCaseObject import BaseUseCaseObject
from interface_adapters.repositories.user import UserRepoFactory
from domain.validators.user.ExistsUserIdentificationValidator import ExistsUserIdentificationValidatorFactory

class GetUserByIdentification(BaseUseCaseObject):
    
    def __init__(self, user_repo, exists_user_identification_validator):
        self.user_repo = user_repo
        self.exists_user_identification_validator = exists_user_identification_validator

    def set_params(self, identification):
        self.identification = identification
        return self
    
    def execute(self):
        self.exists_user_identification_validator.set_params(self.identification).execute()
        return self.user_repo.get_user_by_identification(self.identification)

class GetUserByIdentificationFactory(object):

    @staticmethod
    def get():
        user_repo = UserRepoFactory.get()
        exists_user_identification_validator = ExistsUserIdentificationValidatorFactory.get()
        return GetUserByIdentification(user_repo, exists_user_identification_validator)