#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseUseCaseObject import BaseUseCaseObject
from interface_adapters.repositories.user import UserRepoFactory

class GetTrainersUsers(BaseUseCaseObject):
    
    def __init__(self, user_repo):
        self.user_repo = user_repo
    
    def set_params(self, active):
        self.active = active
        return self

    def execute(self):
        return self.user_repo.get_trainers_users(self.active)


class GetTrainersUsersFactory(object):

    @staticmethod
    def get():
        user_repo = UserRepoFactory.get()
        return GetTrainersUsers(user_repo)