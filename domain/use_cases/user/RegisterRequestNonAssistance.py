#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseUseCaseObject import BaseUseCaseObject
from domain.validators.user.UserIsTrainerValidator import UserIsTrainerValidatorFactory
from domain.validators.common.RangeDatesValidator import RangeDatesValidatorFactory
from interface_adapters.repositories.request_non_assistance import RequestNonAssistanceRepoFactory

class RegisterRequestNonAssistance(BaseUseCaseObject):
    
    def __init__(self, request_non_assistance_repo, user_is_trainer_validator, range_dates_validator):
        self.request_non_assistance_repo = request_non_assistance_repo
        self.user_is_trainer_validator = user_is_trainer_validator
        self.range_dates_validator = range_dates_validator

    def set_params(self, trainer_user_identification, start_at, end_at, comment):
        self.trainer_user_identification = trainer_user_identification
        self.start_at = start_at
        self.end_at = end_at
        self.comment = comment
        self.status = self.request_non_assistance_repo.PENDING_STATUS
        return self
    
    def execute(self):
        self.user_is_trainer_validator.set_params(self.trainer_user_identification).execute()
        self.range_dates_validator.set_params(self.start_at, self.end_at).execute()
        return self.request_non_assistance_repo.register_request_non_assistance(
            self.trainer_user_identification,
            self.start_at,
            self.end_at,
            self.comment,
            self.status
        )

class RegisterRequestNonAssistanceFactory(object):

    @staticmethod
    def get():
        request_non_assistance_repo = RequestNonAssistanceRepoFactory.get()
        user_is_trainer_validator = UserIsTrainerValidatorFactory.get()
        range_dates_validator = RangeDatesValidatorFactory.get()
        return RegisterRequestNonAssistance(request_non_assistance_repo, user_is_trainer_validator, range_dates_validator)