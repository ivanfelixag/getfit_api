#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseUseCaseObject import BaseUseCaseObject
from domain.validators.user.StatusRequestNonAssistanceValidator import StatusRequestNonAssistanceValidatorFactory
from domain.validators.user.RequestNonAssistanceIsPendingValidator import RequestNonAssistanceIsPendingValidatorFactory
from domain.validators.common.RangeDatesValidator import RangeDatesValidatorFactory
from interface_adapters.repositories.request_non_assistance import RequestNonAssistanceRepoFactory

class ChangeStatusRequestNonAssistance(BaseUseCaseObject):
    
    def __init__(self, request_non_assistance_repo, 
        request_non_assistance_is_pending_validator, status_request_non_assistance_validator):
        self.request_non_assistance_repo = request_non_assistance_repo
        self.request_non_assistance_is_pending_validator = request_non_assistance_is_pending_validator
        self.status_request_non_assistance_validator = status_request_non_assistance_validator

    def set_params(self, request_non_assistance_id, status):
        self.request_non_assistance_id = request_non_assistance_id
        self.status = status
        return self
    
    def execute(self):
        request_non_assistance = self.request_non_assistance_repo.get_request_non_assistance_by_id(self.request_non_assistance_id)
        self.request_non_assistance_is_pending_validator.set_params(request_non_assistance.status).execute()
        self.status_request_non_assistance_validator.set_params(self.status).execute()
        self.request_non_assistance_repo.update_status_request_non_assistance(request_non_assistance.id, self.status)
        request_non_assistance.status = self.status
        return request_non_assistance

class ChangeStatusRequestNonAssistanceFactory(object):

    @staticmethod
    def get():
        request_non_assistance_repo = RequestNonAssistanceRepoFactory.get()
        request_non_assistance_is_pending_validator = RequestNonAssistanceIsPendingValidatorFactory.get()
        status_request_non_assistance_validator = StatusRequestNonAssistanceValidatorFactory.get()
        return ChangeStatusRequestNonAssistance(request_non_assistance_repo, 
            request_non_assistance_is_pending_validator, 
            status_request_non_assistance_validator)