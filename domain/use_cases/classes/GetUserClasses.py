#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseUseCaseObject import BaseUseCaseObject
from interface_adapters.repositories.classes import ClassRepoFactory
from domain.validators.user.ExistsUserIdentificationValidator import ExistsUserIdentificationValidatorFactory

class GetUserClasses(BaseUseCaseObject):

    def __init__(self, class_repo, exists_user_identification_validator):
        self.class_repo = class_repo
        self.exists_user_identification_validator = exists_user_identification_validator
    
    def set_params(self, user_identification):
        self.user_identification = user_identification
        return self
    
    def execute(self):
        self.exists_user_identification_validator.set_params(self.user_identification).execute()
        return self.class_repo.get_user_classes(self.user_identification)
    

class GetUserClassesFactory(object):

    @staticmethod
    def get():
        class_repo = ClassRepoFactory.get()
        exists_user_identification_validator = ExistsUserIdentificationValidatorFactory.get()
        return GetUserClasses(class_repo, exists_user_identification_validator)