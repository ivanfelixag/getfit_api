#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ..BaseUseCaseObject import BaseUseCaseObject
from interface_adapters.repositories.classes import ClassRepoFactory

class GetClasses(BaseUseCaseObject):

    def __init__(self, class_repo):
        self.class_repo = class_repo

    def execute(self):
        return self.class_repo.get_all_classes()


class GetClassesFactory(object):

    @staticmethod
    def get():
        class_repo = ClassRepoFactory.get()
        return GetClasses(class_repo)