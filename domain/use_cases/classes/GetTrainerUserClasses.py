#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseUseCaseObject import BaseUseCaseObject
from interface_adapters.repositories.classes import ClassRepoFactory
from domain.validators.user.ExistsUserIdentificationValidator import ExistsUserIdentificationValidatorFactory
from domain.validators.user.UserIsTrainerValidator import UserIsTrainerValidatorFactory

class GetTrainerUserClasses(BaseUseCaseObject):

    def __init__(self, class_repo, exists_user_identification_validator, user_is_trainer_validator):
        self.class_repo = class_repo
        self.exists_user_identification_validator = exists_user_identification_validator
        self.user_is_trainer_validator = user_is_trainer_validator
    
    def set_params(self, user_identification):
        self.user_identification = user_identification
        return self
    
    def execute(self):
        self.exists_user_identification_validator.set_params(self.user_identification).execute()
        self.user_is_trainer_validator.set_params(self.user_identification).execute()
        return self.class_repo.get_trainer_user_classes(self.user_identification)
    

class GetTrainerUserClassesFactory(object):

    @staticmethod
    def get():
        class_repo = ClassRepoFactory.get()
        exists_user_identification_validator = ExistsUserIdentificationValidatorFactory.get()
        user_is_trainer_validator = UserIsTrainerValidatorFactory.get()
        return GetTrainerUserClasses(class_repo, exists_user_identification_validator, user_is_trainer_validator)