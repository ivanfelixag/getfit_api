#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ..BaseUseCaseObject import BaseUseCaseObject
from interface_adapters.managers.transaction import TransactionManager
from interface_adapters.repositories.classes import ClassRepoFactory
from domain.use_cases.session.RegisterSession import RegisterSessionFactory

class RegisterClass(BaseUseCaseObject):

    def __init__(self, class_repo, register_session_use_case):
        self.class_repo = class_repo
        self.register_session_use_case = register_session_use_case
        pass

    def set_params(self, description, sessions, active):
        self.description = description
        self.sessions = sessions
        self.active = active
        return self

    def execute(self):
        TransactionManager.begin_transaction()
        try:
            new_class = self.class_repo.register_class(self.description, self.active)
            for session in self.sessions:
                new_session = self.register_session_use_case.set_params(session.day, session.start_time, session.end_time, session.capacity, 
                        session.trainer_user.identification, new_class.id).execute()
                new_class.sessions.append(new_session)
            return new_class
        except Exception as ex:
            TransactionManager.do_roolback()
            raise ex
        else:
            TransactionManager.do_commit()
        finally:
            TransactionManager.end_transaction()

class RegisterClassFactory(object):

    @staticmethod
    def get():
        class_repo = ClassRepoFactory.get()
        register_session_use_case = RegisterSessionFactory.get()
        return RegisterClass(class_repo, register_session_use_case)