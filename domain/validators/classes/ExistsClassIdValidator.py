#!/usr/bin/env python
# -*- coding: utf-8 -*-
from interface_adapters.repositories.classes import ClassRepoFactory
from common.exceptions.classes.ClassNotFoundException import ClassNotFoundException

class ExistsClassIdValidator(object):

    def __init__(self, class_repo):
        self.class_repo = class_repo
    
    def set_params(self, id):
        self.id = id
        return self
    
    def execute(self):
        exists = self.class_repo.exists_class_by_id(self.id)
        if not exists:
            raise ClassNotFoundException()

class ExistsClassIdValidatorFactory(object):

    @staticmethod
    def get():
        class_repo = ClassRepoFactory.get()
        return ExistsClassIdValidator(class_repo)