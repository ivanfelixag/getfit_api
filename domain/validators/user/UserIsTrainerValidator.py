#!/usr/bin/env python
# -*- coding: utf-8 -*-
from interface_adapters.repositories.user import UserRepoFactory
from common.exceptions.user.UserHasNotRoleException import UserHasNotRoleException

class UserIsTrainerValidator(object):

    def __init__(self, user_repo):
        self.user_repo = user_repo
    
    def set_params(self, identification):
        self.identification = identification
        return self
    
    def execute(self):
        exists = self.user_repo.user_has_role(self.identification, self.user_repo.TRAINER_ROLE)
        if not exists:
            raise UserHasNotRoleException(self.identification, self.user_repo.TRAINER_ROLE)

class UserIsTrainerValidatorFactory(object):

    @staticmethod
    def get():
        user_repo = UserRepoFactory.get()
        return UserIsTrainerValidator(user_repo)