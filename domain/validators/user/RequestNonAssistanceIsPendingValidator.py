#!/usr/bin/env python
# -*- coding: utf-8 -*-
from interface_adapters.repositories.request_non_assistance import RequestNonAssistanceRepoFactory
from common.exceptions.user.RequestNonAssistanceIsNotPendingException import RequestNonAssistanceIsNotPendingException

class RequestNonAssistanceIsPendingValidator(object):

    def __init__(self, request_non_assistance_repo):
        self.request_non_assistance_repo = request_non_assistance_repo
    
    def set_params(self, status):
        self.status = status
        return self
    
    def execute(self):
        if self.status != self.request_non_assistance_repo.PENDING_STATUS:
            raise RequestNonAssistanceIsNotPendingException()

class RequestNonAssistanceIsPendingValidatorFactory(object):

    @staticmethod
    def get():
        request_non_assistance_repo = RequestNonAssistanceRepoFactory.get()
        return RequestNonAssistanceIsPendingValidator(request_non_assistance_repo)