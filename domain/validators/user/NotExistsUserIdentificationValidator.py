#!/usr/bin/env python
# -*- coding: utf-8 -*-
from interface_adapters.repositories.user import UserRepoFactory
from common.exceptions.user.UserAlreadyExistsException import UserAlreadyExistsException

class NotExistsUserIdentificationValidator(object):

    def __init__(self, user_repo):
        self.user_repo = user_repo
    
    def set_params(self, identification, check_active=True):
        self.identification = identification
        self.check_active = check_active
        return self
    
    def execute(self):
        exists = self.user_repo.exists_user_identification(self.identification, self.check_active)
        if exists:
            raise UserAlreadyExistsException(self.identification)

class NotExistsUserIdentificationValidatorFactory(object):

    @staticmethod
    def get():
        user_repo = UserRepoFactory.get()
        return NotExistsUserIdentificationValidator(user_repo)