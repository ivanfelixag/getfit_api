#!/usr/bin/env python
# -*- coding: utf-8 -*-
from interface_adapters.repositories.user import UserRepoFactory
from common.exceptions.user.UserHasNotRoleException import UserHasNotRoleException

class UserHasRoleValidator(object):

    def __init__(self, user_repo):
        self.user_repo = user_repo
    
    def set_params(self, identification, role):
        self.identification = identification
        self.role = role
        return self
    
    def execute(self):
        exists = self.user_repo.user_has_role(self.identification, self.role)
        if not exists:
            raise UserHasNotRoleException(self.identification, self.role)

class UserHasRoleValidatorFactory(object):

    @staticmethod
    def get():
        user_repo = UserRepoFactory.get()
        return UserHasRoleValidator(user_repo)