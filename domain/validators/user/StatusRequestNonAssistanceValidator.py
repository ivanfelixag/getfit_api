#!/usr/bin/env python
# -*- coding: utf-8 -*-
from interface_adapters.repositories.request_non_assistance import RequestNonAssistanceRepoFactory
from common.exceptions.user.StatusRequestNonAssistanceException import StatusRequestNonAssistanceException

class StatusRequestNonAssistanceValidator(object):

    def __init__(self, request_non_assistance_repo):
        self.request_non_assistance_repo = request_non_assistance_repo
    
    def set_params(self, status):
        self.status = status
        return self
    
    def execute(self):
        if self.status not in [
            self.request_non_assistance_repo.PENDING_STATUS,
            self.request_non_assistance_repo.APPROVED_STATUS,
            self.request_non_assistance_repo.DENIED_STATUS
        ]:
            raise StatusRequestNonAssistanceException()

class StatusRequestNonAssistanceValidatorFactory(object):

    @staticmethod
    def get():
        request_non_assistance_repo = RequestNonAssistanceRepoFactory.get()
        return StatusRequestNonAssistanceValidator(request_non_assistance_repo)