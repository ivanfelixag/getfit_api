#!/usr/bin/env python
# -*- coding: utf-8 -*-
from interface_adapters.repositories.user import UserRepoFactory
from common.exceptions.user.UserNotFoundException import UserNotFoundException

class ExistsUserIdentificationValidator(object):

    def __init__(self, user_repo):
        self.user_repo = user_repo
    
    def set_params(self, identification):
        self.identification = identification
        return self
    
    def execute(self):
        exists = self.user_repo.exists_user_identification(self.identification)
        if not exists:
            raise UserNotFoundException(self.identification)

class ExistsUserIdentificationValidatorFactory(object):

    @staticmethod
    def get():
        user_repo = UserRepoFactory.get()
        return ExistsUserIdentificationValidator(user_repo)