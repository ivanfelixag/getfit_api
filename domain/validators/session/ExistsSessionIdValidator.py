#!/usr/bin/env python
# -*- coding: utf-8 -*-
from interface_adapters.repositories.session import SessionRepoFactory
from common.exceptions.session.SessionNotFoundException import SessionNotFoundException

class ExistsSessionIdValidator(object):

    def __init__(self, session_repo):
        self.session_repo = session_repo
    
    def set_params(self, id):
        self.id = id
        return self
    
    def execute(self):
        exists = self.session_repo.exists_session_by_id(self.id)
        if not exists:
            raise SessionNotFoundException()

class ExistsSessionIdValidatorFactory(object):

    @staticmethod
    def get():
        session_repo = SessionRepoFactory.get()
        return ExistsSessionIdValidator(session_repo)