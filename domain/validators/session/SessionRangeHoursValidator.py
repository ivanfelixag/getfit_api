#!/usr/bin/env python
# -*- coding: utf-8 -*-
from interface_adapters.repositories.session import SessionRepoFactory
from common.exceptions.session.SessionStartTimeException import SessionStartTimeException
from common.exceptions.session.SessionMinimumMinutesException import SessionMinimumMinutesException

class SessionRangeHoursValidator(object):

    def __init__(self, session_repo):
        self.session_repo = session_repo

    def set_params(self, start_time, end_time):
        self.start_time = start_time
        self.end_time = end_time
        return self
    
    def execute(self):
        if self.end_time < self.start_time:
            raise SessionStartTimeException()
        
        delta_minutes = (self.end_time.hour - self.start_time.hour)*60
        delta_minutes += self.end_time.minute - self.start_time.minute
        delta_minutes += (self.end_time.second - self.start_time.second)/60.0

        if delta_minutes < self.session_repo.MINIMUM_MINUTES:
            raise SessionMinimumMinutesException(self.session_repo.MINIMUM_MINUTES)

class SessionRangeHoursValidatorFactory(object):

    @staticmethod
    def get():
        session_repo = SessionRepoFactory.get()
        return SessionRangeHoursValidator(session_repo)