#!/usr/bin/env python
# -*- coding: utf-8 -*-
from interface_adapters.repositories.session import SessionRepoFactory
from common.exceptions.session.UserAlreadyInSessionException import UserAlreadyInSessionException

class NotExistsParticipantSessionValidator(object):

    def __init__(self, session_repo):
        self.session_repo = session_repo
    
    def set_params(self, session_id, user_identification):
        self.session_id = session_id
        self.user_identification = user_identification
        return self
    
    def execute(self):
        exists = self.session_repo.exists_participant_session(self.session_id, self.user_identification)
        if exists:
            raise UserAlreadyInSessionException()

class NotExistsParticipantSessionValidatorFactory(object):

    @staticmethod
    def get():
        session_repo = SessionRepoFactory.get()
        return NotExistsParticipantSessionValidator(session_repo)