#!/usr/bin/env python
# -*- coding: utf-8 -*-
from interface_adapters.repositories.session import SessionRepoFactory
from common.exceptions.session.SessionNotEnoughCapacityException import SessionNotEnoughCapacityException

class SessionHasCapacityValidator(object):

    def __init__(self, session_repo):
        self.session_repo = session_repo
    
    def set_params(self, id):
        self.id = id
        return self
    
    def execute(self):
        session = self.session_repo.get_session_by_id(self.id)
        if session.capacity == 0:
            raise SessionNotEnoughCapacityException()

class SessionHasCapacityValidatorFactory(object):

    @staticmethod
    def get():
        session_repo = SessionRepoFactory.get()
        return SessionHasCapacityValidator(session_repo)