#!/usr/bin/env python
# -*- coding: utf-8 -*-
from interface_adapters.repositories.session import SessionRepoFactory
from common.exceptions.session.SessionDayNotFoundException import SessionDayNotFoundException

class ExistsSessionDayValidator(object):

    def __init__(self, session_repo):
        self.session_repo = session_repo
    
    def set_params(self, day):
        self.day = day
        return self
    
    def execute(self):
        if not self.day in self.session_repo.DAYS:
            raise SessionDayNotFoundException(self.day)

class ExistsSessionDayValidatorFactory(object):

    @staticmethod
    def get():
        session_repo = SessionRepoFactory.get()
        return ExistsSessionDayValidator(session_repo)