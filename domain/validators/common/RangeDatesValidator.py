#!/usr/bin/env python
# -*- coding: utf-8 -*-
from interface_adapters.repositories.session import SessionRepoFactory
from common.exceptions.common.RangeDatesException import RangeDatesException

class RangeDatesValidator(object):

    def set_params(self, start_at, end_at):
        self.start_at = start_at
        self.end_at = end_at
        return self
    
    def execute(self):
        if self.end_at < self.start_at:
            raise RangeDatesException()

class RangeDatesValidatorFactory(object):

    @staticmethod
    def get():
        return RangeDatesValidator()