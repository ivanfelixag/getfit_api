#!/usr/bin/env python
# -*- coding: utf-8 -*-

class HistoricalPeriodTrainerSession(object):

    def __init__(self, id, session_id, trainer_user_identification, start_date, end_date, temporal_substitution):
        self._id = id
        self._session_id = session_id
        self._trainer_user_identification = trainer_user_identification
        self._start_date = start_date
        self._end_date = end_date
        self._temporal_substitution = temporal_substitution
    
    @property
    def id(self):
        return self._id
    
    @property
    def session_id(self):
        return self._session_id
    
    @property
    def trainer_user_identification(self):
        return self._trainer_user_identification
    
    @property
    def start_date(self):
        return self._start_date

    @property
    def end_date(self):
        return self._end_date
    
    @property
    def temporal_substitution(self):
        return self._temporal_substitution