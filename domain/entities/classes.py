#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Class(object):
    
    def __init__(self, id, description, sessions, active):
        self._id = id
        self._description = description
        self._sessions = sessions
        self._active = active

    @property
    def id(self):
        return self._id

    @property
    def description(self):
        return self._description
    
    @property
    def sessions(self):
        return self._sessions

    @property
    def active(self):
        return self._active