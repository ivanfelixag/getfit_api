#!/usr/bin/env python
# -*- coding: utf-8 -*-

class GymInformation(object):
    
    def __init__(self, id, title, description, created_at):
        self._id = id
        self._title = title
        self._description = description
        self._created_at = created_at

    @property
    def id(self):
        return self._id

    @property
    def title(self):
        return self._title
    
    @property
    def description(self):
        return self._description
    
    @property
    def created_at(self):
        return self._created_at