#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Session(object):
    
    def __init__(self, id, day, start_time, end_time, capacity, trainer_user):
        self._id = id
        self._day = day
        self._start_time = start_time
        self._end_time = end_time
        self._capacity = capacity
        self._trainer_user = trainer_user if trainer_user and trainer_user.active else None

    @property
    def id(self):
        return self._id

    @property
    def day(self):
        return self._day
    
    @property
    def start_time(self):
        return self._start_time

    @property
    def end_time(self):
        return self._end_time

    @property
    def capacity(self):
        return self._capacity

    @property
    def trainer_user(self):
        return self._trainer_user