#!/usr/bin/env python
# -*- coding: utf-8 -*-

class RequestNonAssistance(object):
    
    def __init__(self, id, trainer_user_identification, start_at, end_at, comment, status = 'pending'):
        self._id = id
        self._trainer_user_identification = trainer_user_identification
        self._start_at = start_at
        self._end_at = end_at
        self._comment = comment
        self._status = status

    @property
    def id(self):
        return self._id

    @property
    def trainer_user_identification(self):
        return self._trainer_user_identification

    @property
    def start_at(self):
        return self._start_at
    
    @property
    def end_at(self):
        return self._end_at
    
    @property
    def comment(self):
        return self._comment

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        self._status = status
