#!/usr/bin/env python
# -*- coding: utf-8 -*-

class User(object):
    
    def __init__(self, first_name='', last_name='', email='', identification='', password='', phone_number='', active=True, roles=[]):
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._identification = identification
        self._password = password
        self._phone_number = phone_number
        self._active = active
        self._roles = roles

    @property
    def first_name(self):
        return self._first_name

    @property
    def last_name(self):
        return self._last_name

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    @property
    def email(self):
        return self._email

    @property
    def identification(self):
        return self._identification
    
    @property
    def password(self):
        return self._password

    @property
    def phone_number(self):
        return self._phone_number

    @property
    def active(self):
        return self._active

    @property
    def roles(self):
        return self._roles