## Locks in a clean architecture

Here is my vision of how a Lock should be implemented in a clean architecture:

- In order to establish a clear communication between the Framework layer and the Interface Adapters layer, it is necessary to define an [interface](../common/interfaces/ILock.py) that defines the contract between both:
```
class ILock:
    __metaclass__ = ABCMeta

    @abstractmethod
    def begin_lock(cls, name): raise NotImplementedError
```
- After this, it is necessary to create in the Framework layer a [class](../external_interfaces/managers/lock.py) that establishes the connection with our Locks system. In this case I have used Redis to share the lock between the different processes that are executed with the Gunicorn.
- So that our use cases can use the Lock with total independence of the mechanism that has been implemented, it is necessary to create a [class](../interface_adapters/managers/lock.py) in the Interface Adapters layer that serves as a bridge.
- As you can see in the use case [RegisterParticipantSession](../domain/use_cases/session/RegisterParticipantSession.py#L32), the lock is implemented to make a section of our business logic atomic:
```
with LockManager.begin_lock(f"LOCK_PARTICIPANT_SESSION_{self.session_id}"):
     # Atomic section
```