To access the Redis monitor, follow the steps below:
```
docker exec -it getfit_api_cache bash
redis-cli monitor
```