## Transactions in a clean architecture

My interpretation of how transactions should be implemented in a clean architecture is:

- Create an [interface](../common/interfaces/ITransaction.py) that defines what methods Managers must implement in both the Framework layer and the Interface Adapter layer:
```
class ITransaction:
    __metaclass__ = ABCMeta

    @abstractmethod
    def begin_transaction(cls): raise NotImplementedError

    @abstractmethod
    def do_roolback(cls): raise NotImplementedError

    @abstractmethod
    def do_commit(cls): raise NotImplementedError

    @abstractmethod
    def end_transaction(cls): raise NotImplementedError
```
- Create a [class](../external_interfaces/managers/transaction.py) in the Framework layer that allows us to implement the interface and communicate, as it is in Django, with the Framework Transaction class.
- Create another [class](../interface_adapters/managers/transaction.py) in the Interface Adapter layer that communicates with the previous Transaction class. In this way our use cases can make use of it.
- As you can see in the use case [RegisterParticipantSession](../domain/use_cases/session/RegisterParticipantSession.py#L36), the transaction is implemented.
My vision is that the use case should control the transaction as part of its business logic:
```
TransactionManager.begin_transaction()
try:
    # Your logic
except Exception as ex:
    TransactionManager.do_roolback()
    raise ex
else:
    TransactionManager.do_commit()
finally:
    TransactionManager.end_transaction()
```