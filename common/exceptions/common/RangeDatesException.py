#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseException import BaseException
from django.utils.translation import gettext as _

class RangeDatesException(BaseException):
    def __init__(self):
        super().__init__(_("The start date must be less than the end date"))