#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseException import BaseException
from django.utils.translation import gettext as _

class SessionMinimumMinutesException(BaseException):
    def __init__(self, minimum_minutes):
        super().__init__(_(f"Sessions must last at least {minimum_minutes} minutes"))