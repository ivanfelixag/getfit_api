#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseException import BaseException
from django.utils.translation import gettext as _

class SessionStartTimeException(BaseException):
    def __init__(self):
        super().__init__(_("The start time must be less than the end time"))