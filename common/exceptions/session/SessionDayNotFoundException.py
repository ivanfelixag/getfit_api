#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseException import BaseException
from django.utils.translation import gettext as _

class SessionDayNotFoundException(BaseException):
    def __init__(self, day):
        super().__init__(_(f"The {day} day identifier does not exist"))