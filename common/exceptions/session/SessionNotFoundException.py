#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseException import BaseException
from django.utils.translation import gettext as _

class SessionNotFoundException(BaseException):
    def __init__(self):
        super().__init__(_("There is not a session with that id"))