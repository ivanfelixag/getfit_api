#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseException import BaseException
from django.utils.translation import gettext as _

class UserNotInSessionException(BaseException):
    def __init__(self):
        super().__init__(_("The user is not registered in the session"))