#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseException import BaseException
from django.utils.translation import gettext as _

class RequestNonAssistanceIsNotPendingException(BaseException):
    def __init__(self):
        super().__init__(_(f"The status field for the request non assistence has already been modified"))