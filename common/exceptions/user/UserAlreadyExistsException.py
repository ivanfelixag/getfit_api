#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseException import BaseException
from django.utils.translation import gettext as _

class UserAlreadyExistsException(BaseException):
    def __init__(self, identification):
        super().__init__(_(f"There is already a user with the identification {identification}"))