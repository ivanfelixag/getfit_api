#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseException import BaseException
from django.utils.translation import gettext as _

class StatusRequestNonAssistanceException(BaseException):
    def __init__(self):
        super().__init__(_(f"The status is not an allowed value"))