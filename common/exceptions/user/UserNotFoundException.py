#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseException import BaseException
from django.utils.translation import gettext as _

class UserNotFoundException(BaseException):
    def __init__(self, identification):
        super().__init__(_(f"There is no user with the identification {identification}"))