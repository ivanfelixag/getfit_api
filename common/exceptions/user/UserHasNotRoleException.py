#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ..BaseException import BaseException
from django.utils.translation import gettext as _

class UserHasNotRoleException(BaseException):
    def __init__(self, identification, role):
        super().__init__(_(f"The user with the identification {identification} is not a {role}"))