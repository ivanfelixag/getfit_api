#!/usr/bin/env python
# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod

class IMailingStrategy:
    __metaclass__ = ABCMeta

    @property
    @abstractmethod
    def mail_id(self): raise NotImplementedError

    @abstractmethod
    def send(self): raise NotImplementedError