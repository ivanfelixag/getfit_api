#!/usr/bin/env python
# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod

class ILock:
    __metaclass__ = ABCMeta

    @abstractmethod
    def begin_lock(cls, name): raise NotImplementedError