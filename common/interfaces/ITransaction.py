#!/usr/bin/env python
# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod

class ITransaction:
    __metaclass__ = ABCMeta

    @abstractmethod
    def begin_transaction(cls): raise NotImplementedError

    @abstractmethod
    def do_roolback(cls): raise NotImplementedError

    @abstractmethod
    def do_commit(cls): raise NotImplementedError

    @abstractmethod
    def end_transaction(cls): raise NotImplementedError